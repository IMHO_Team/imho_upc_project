//
//  Defaults.swift
//  IMHO
//
//  Created by Rafael Zamora on 3/8/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import Foundation
import UIKit

// MARK: - Colors
let kAppMainColor:UIColor = UIColor(red: 192/255.0, green: 23/255.0, blue: 68/255.0, alpha: 1)
let kAppSecondaryColor:UIColor = UIColor(red: 36/255, green: 181/255, blue: 140/255, alpha: 1)
let kAppTextColor:UIColor = UIColor(red: 146/255, green: 146/255, blue: 146/255, alpha: 1)
let kAppLoginFBColor:UIColor = UIColor(red: (59/255.0), green: (89/255.0), blue: (152/255.0), alpha: 1)
let kAppLoginGoogleColor:UIColor = UIColor(red: (211/255.0), green: (72/255.0), blue: (54/255.0), alpha: 1)

// MARK : - Share

let kAppWebsite:String = "http://www.imhoapp.es"

// MARK: - NSNotifications

let kNotificationAnswerDidPost:String = "com.imho.app.notificationanswerdidpost"
let kNotificationDidSelectCategory:String = "com.imho.app.notificationdidselectcategory"
let kNotificationLoginSwitchesPan:String = "com.imho.app.notificationloginswitchespan"
let kNotificationDidSelectAddFriend:String = "com.imhoapp.notificationdidselectaddfriend"
let kNotificationDidAddUsersToMission:String = "com.imhoapp.notificationdidadduserstomission"
let kNotificationForwardTapped:String = "com.imhoapp.notificationforwardtapped"
let kNotificationDidForwardMission:String = "com.imhoapp.notificationdidforwardmission"
let kNotificationShareTapped:String = "com.imhoapp.notificationsharetapped"
let kNotificationMoreTapped:String = "com.imhoapp.notificationmoretapped"
let kNotificationSaveMissionForLater:String = "com.imhoapp.notificationsavemissionforlater"
let kNotificationDidSelectMission:String = "com.imhoapp.notificationdidselectmission"
let kNotificationDidSelectComment:String = "com.imhoapp.didselectcomment"
let kNotificationDidAddComment:String = "com.imhoapp.didaddcomment"
let kNotificationCreateMission:String = "com.imhoapp.es.notificationcreatemission"

// MARK: - Parse

let kParseAppId:String = "oVLdL2H4I49O698yKuXgFn6PJl02R5lBNSdq39pQ"
let kParseClientId:String = "olGSL4SkJzKuxyKcjeKpdAe9GPAFpNmyYWnSvNa0"

// MARK: - Parse
let kParseCloudFuncionLastAnswer = "closeMissionWithLastAnswer"

// MARK: - Values

let kChannelPrefix:String = "CH"

// MARK: - AWS

let kS3BucketName:String = "imhoapp.images"//arn:aws:s3:::es.imhoapp.imho/*

let kAWSCredentialProvider = AWSCognitoCredentialsProvider(regionType: AWSRegionType.EUWest1,      identityId: nil, accountId: "342712166719", identityPoolId: "eu-west-1:da3ee9ea-5819-4888-9669-0d91b77319b4", unauthRoleArn: "arn:aws:iam::342712166719:role/Cognito_IMHOUnauth_Role", authRoleArn: "arn:aws:iam::342712166719:role/Cognito_IMHOAuth_Role", logins: nil)

let kAWSDefaultServiceConfiguration = AWSServiceConfiguration(region: AWSRegionType.EUWest1, credentialsProvider: kAWSCredentialProvider)

let kDefaultImagesURL = "http://"+kS3BucketName+".s3.amazonaws.com/"
let kDefaultContentType = "image/png"

// MARK: - Mission class
let kParseClassMission:String = "Mission"
let kParseVariableMissionObjectId:String = "objectId"
let kParseVariableMissionQuestion:String = "question"
let kParseVariableMissionCreatedAt:String = "createdAt"
let kParseVariableMissionLength:String = "length"
let kParseVariableMissionDateFinished:String = "dateFinished"
let kParseVariableMissionSingleUsers:String = "singleUsers"
let kParseVariableMissionImages:String = "images"
let kParseVariableMissionGroups:String = "groups"
let kParseVariableMissionStatus:String = "status"
let kParseVariableMissionUser:String = "user"
let kParseVariableMissionVisible:String = "visible"
let kParseVariableMissionGeopoint:String = "geopoint"
let kParseVariableMissionCategory:String = "category"
let kParseVariableMissionAnswers:String = "answers"
let kParseVariableMissionUserAnswer:String = "userAnswer"
let kParseVariableMissionIsPrivate:String = "isPrivate"
let kParseVariableMissionIsSecret:String = "isSecret"
let kParseVariableMissionImage1:String = "image1"
let kParseVariableMissionImage2:String = "image2"

// MARK: MissionStatus

let kParseValueMissionStatusRunning:String = "running"
let kParseValueMissionStatusFinished:String = "finished"
let kParseValueMissionStatusClosed:String = "closed"

// MARK: Image class
let kParseClassImage:String = "Image"
let kParseVariableImageType:String = "type"
let kParseVariableImageFile:String = "file"
let kParseVariableImageFileName:String = "fileName"
let kParseVariableImageStatus:String = "status"

// MARK: Answer class

let kParseClassAnswer:String = "Answer"
let kParseVariableAnswerUser:String = "user"
let kParseVariableAnswerMission:String = "mission"
let kParseVariableAnswerMissionStatus:String = "mission.status"
let kParseVariableAnswerComment:String = "comment"
let kParseVariableAnswerSelectedImage:String = "selectedImage"
let kParseVariableAnswerSingleAnswer:String = "singleAnswer"
let kParseVariableAnswerPoints:String = "points"

// MARK: Category class

let kParseClassCategory:String = "Category"
let kParseVariableCategoryName:String = "name"
let kParseVariableCategoryImage:String = "image"

// MARK: User Class
let kParseClassUser:String = "User"
let kParseVariableUserObjectId:String = "objectId"
let kParseVariableUserFacebookId:String = "facebookId"
let kParseVariableUserUsername:String = "username"
let kParseVariableUserEmail:String = "email"
let kParseVariableUserName:String = "name"
let kParseVariableUserGender:String = "gender"
let kParseVariableUserBirthDate:String = "birthdate"
let kParseVariableUserCountry:String = "country"
let kParseVariableUserCity:String = "city"
let kParseVariableUserWebPage:String = "web"
let kParseVariableUserRankPoints:String = "rankPoints"
let kParseVariableUserFriends:String = "friends"
let kParseVariableUserProfileImage:String = "profileImage"


// MARK: - Fonts
func helveticaNeueLightWithSize(size: CGFloat) -> UIFont {
    
    return UIFont(name: "HelveticaNeue-Light", size: size)!
}

// MARK: - Dates

func dateDiff(origDate:NSDate) {
    
}

// MARK: - Sizes

let kHomeCardSize:CGRect = CGRectMake(0, 0, 260, 340)

// MARK: - Functions

func getLocalizedStringForKey(key: String) -> String {
    
    return NSBundle.mainBundle().localizedStringForKey(key, value: "", table: nil)
}

func presentShareOnViewController(viewController:UIViewController, forMission mission:MissionModel) {
    
    let textItem:NSString = NSString(format: getLocalizedStringForKey("socialShareText"),mission.question!)
    let urlItem:NSURL = NSURL(string: kAppWebsite)!
    var imageItem:UIImage?
    
    if ((mission.images!.objectAtIndex(0) as! ImageModel).image != nil) {
        imageItem = (mission.images!.objectAtIndex(0) as! ImageModel).image!
    }
    
    let activityViewController:UIActivityViewController?
    
    if (imageItem != nil) {
        activityViewController = UIActivityViewController(activityItems: [textItem, urlItem, imageItem!], applicationActivities: nil)
    } else {
        activityViewController = UIActivityViewController(activityItems: [textItem, urlItem], applicationActivities: nil)
    }
    
    activityViewController!.excludedActivityTypes = [
        UIActivityTypePostToWeibo,
        UIActivityTypePrint,
        UIActivityTypeAssignToContact,
        UIActivityTypeSaveToCameraRoll,
        UIActivityTypeAddToReadingList,
        UIActivityTypePostToFlickr,
        UIActivityTypePostToVimeo,
        UIActivityTypePostToTencentWeibo
    ]
    
    viewController.presentViewController(activityViewController!, animated: true, completion: nil)
}

func doOnce(block:() -> ()) {
    struct TokenHolder {
        static var token: dispatch_once_t = 0;
    }
    
    dispatch_once(&TokenHolder.token) {
        // Code!
        block()
    }
}

func dateFromString(dateString:String, dateFormat:String) -> NSDate {
    
    let dateFormatter:NSDateFormatter = NSDateFormatter()    
    
    dateFormatter.dateFormat = dateFormat
    
    return dateFormatter.dateFromString(dateString)!
    
}

func downloadImageWithURL(url:NSURL, andSuccessBlock successBlock:(succeeded:Bool,image:UIImage) ->(), andErrorBlock errorBlock:(error:NSError) ->()) {
    
    let request:NSMutableURLRequest = NSMutableURLRequest(URL: url)
    NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) { (response, data, error) -> Void in
        
        if (error == nil) {
            successBlock(succeeded: true, image: UIImage(data: data!)!)
        } else {
            errorBlock(error: error!)
        }
    }
}