//
//  SegmentedButton.swift
//  IMHO
//
//  Created by Rafael Zamora on 3/21/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import UIKit

@objc protocol SegmentedButtonDelegate {
    
    optional func didSelectButtonAtIndex(index:Int)
}

class SegmentedButton: UIView {

    
    var delegate:SegmentedButtonDelegate?
    
    var selectedIndex:Int = 0
    
    func setupview(titles:NSArray) {
        
        var aFrame:CGRect = self.frame
        aFrame.size.width = self.superview!.frame.size.width
        self.frame = aFrame
        
        // Creates a button for every title on the array
        for var i=0;i<titles.count;i++ {
            // Init button
            let button:UIButton = UIButton(frame: CGRectMake(getXPositionForIndex(i, inCount: titles.count), 0, self.frame.size.width/CGFloat(titles.count), self.frame.size.height))
            // Customize button
            button.setTitle(titles.objectAtIndex(i) as? String, forState: UIControlState.Normal)
            button.setTitle(titles.objectAtIndex(i) as? String, forState: UIControlState.Highlighted)
            button.titleLabel!.font = helveticaNeueLightWithSize(17)
            button.tag = i
            // Add action to button
            button.addTarget(self, action: "didSelectButtonAtIndex:", forControlEvents: UIControlEvents.TouchUpInside)
            
            let selectedBarImageView:UIImageView = UIImageView(frame: CGRectMake(button.frame.origin.x, self.frame.size.height - 3, self.frame.size.width/CGFloat(titles.count), 2))
            selectedBarImageView.backgroundColor = kAppSecondaryColor
            selectedBarImageView.tag = i
            // Set firs button selected
            if (i > 0) {
                selectedBarImageView.alpha = 0
                button.setTitleColor(kAppTextColor, forState: UIControlState.Normal)
            }
            else {
                button.setTitleColor(kAppSecondaryColor, forState: UIControlState.Normal)
            }
            
            self.addSubview(button)
            self.addSubview(selectedBarImageView)
            
        }
        
        let barImageView:UIImageView = UIImageView(frame: CGRectMake(0, self.frame.size.height - 1, self.frame.size.width, 1))
        barImageView.backgroundColor = kAppSecondaryColor
        barImageView.tag = 999
        self.addSubview(barImageView)
        
    }

    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
    }
    
    /**
    Get x position for a button to add
    
    - parameter index: Index of the button
    - parameter count: Number of elements at segmented button
    */
    func getXPositionForIndex (index:Int, inCount count:Int) -> CGFloat {
        
        let flIndex:CGFloat = CGFloat(index)
        let width:CGFloat = self.frame.size.width/CGFloat(count)
        
        return flIndex*width
        
    }
    
    /**
    Change current selection
    
    - parameter index: The index of the selected button
    */
    func setSelectedTab(index:Int) {
        
        for view in self.subviews {
            
            if (view.isKindOfClass(UIImageView) && view.tag == index) {
                UIView.animateWithDuration(0.4, animations: { () -> Void in
                    (view as! UIImageView).alpha = 1
                })
            }
            else if (view.isKindOfClass(UIImageView) && view.tag != 999) {
                UIView.animateWithDuration(0.4, animations: { () -> Void in
                    (view as! UIImageView).alpha = 0
                })
            }
            else if (view.isKindOfClass(UIButton) && view.tag != index) {
                
                UIView.animateWithDuration(1.2, animations: { () -> Void in
                    (view as! UIButton).setTitleColor(kAppTextColor, forState: UIControlState.Normal)
                })
            }
            else if (view.isKindOfClass(UIButton) && view.tag == index) {
                UIView.animateWithDuration(1.2, animations: { () -> Void in
                    (view as! UIButton).setTitleColor(kAppSecondaryColor, forState: UIControlState.Normal)
                })
            }
        }
        
        self.selectedIndex = index
    }
    
    /**
    Called when button pressed
    
    - parameter successBlock: Block executed when friends list returned
    */
    func didSelectButtonAtIndex(button:UIButton) {
        
        for view in self.subviews {
            // Change the horizontal line below the buttons
            if (view.isKindOfClass(UIImageView) && view.tag == button.tag) {
                UIView.animateWithDuration(0.4, animations: { () -> Void in
                    (view as! UIImageView).alpha = 1
                })
            }
            else if (view.isKindOfClass(UIImageView) && view.tag != 999) {
                UIView.animateWithDuration(0.4, animations: { () -> Void in
                    (view as! UIImageView).alpha = 0
                })
            }
            else if (view.isKindOfClass(UIButton) && view.tag != button.tag) {
                
                UIView.animateWithDuration(1.2, animations: { () -> Void in
                    (view as! UIButton).setTitleColor(kAppTextColor, forState: UIControlState.Normal)
                })
            }
        }
        // Change collor of the selected button
        UIView.animateWithDuration(1.2, animations: { () -> Void in
            button.setTitleColor(kAppSecondaryColor, forState: UIControlState.Normal)
        })
        // Change selected index
        self.selectedIndex = button.tag
        // Call delegate's method
        delegate?.didSelectButtonAtIndex!(button.tag)
    }

}
