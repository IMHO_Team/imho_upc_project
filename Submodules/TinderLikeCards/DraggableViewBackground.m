//
//  DraggableViewBackground.m
//  testing swiping
//
//  Created by Richard Kim on 8/23/14.
//  Copyright (c) 2014 Richard Kim. All rights reserved.
//

#import "DraggableViewBackground.h"
#import "IMHO-Swift.h"

@implementation DraggableViewBackground{
    NSInteger cardsLoadedIndex; //%%% the index of the card you have loaded into the loadedCards array last
    NSMutableArray *loadedCards; //%%% the array of card loaded (change max_buffer_size to increase or decrease the number of cards this holds)
    
    UIButton* menuButton;
    UIButton* messageButton;
    UIButton* checkButton;
    UIButton* xButton;
    UIButton*  emptyStateButton;
}
//this makes it so only two cards are loaded at a time to
//avoid performance and memory costs
static const int MAX_BUFFER_SIZE = 2; //%%% max number of cards loaded at any given time, must be greater than 1
//static const float CARD_HEIGHT = 386; //%%% height of the draggable card
//static const float CARD_WIDTH = 290; //%%% width of the draggable card

@synthesize missionArray;
@synthesize allCards;//%%% all the cards

- (id)initWithFrame:(CGRect)frame andArray:(NSMutableArray *)array
{
    self = [super initWithFrame:frame];
    if (self) {
        [super layoutSubviews];
        [self setupView];
        missionArray = array;
        loadedCards = [[NSMutableArray alloc] init];
        allCards = [[NSMutableArray alloc] init];
        cardsLoadedIndex = 0;
        [self loadCards];
        
    }
    return self;
}

//%%% sets up the extra buttons on the screen
-(void)setupView
{
    CGFloat cardWidth = self.frame.size.width*0.8;
    CGFloat cardHeight = self.frame.size.height*0.7;
    CGFloat xPosition = (self.frame.size.width - cardWidth)/2;
    CGFloat yPosition = (self.frame.size.height - cardHeight)/4;
    
    CGFloat buttonSize = self.frame.size.width/5;
    CGFloat buttonYPosition = yPosition + cardHeight + 20;
    
    self.backgroundColor = [UIColor colorWithRed:.92 green:.93 blue:.95 alpha:1]; //the gray background colors
    menuButton = [[UIButton alloc]initWithFrame:CGRectMake(17, 34, 22, 15)];
    [menuButton setImage:[UIImage imageNamed:@"menuButton"] forState:UIControlStateNormal];
    messageButton = [[UIButton alloc]initWithFrame:CGRectMake(284, 34, 18, 18)];
    [messageButton setImage:[UIImage imageNamed:@"messageButton"] forState:UIControlStateNormal];
    xButton = [[UIButton alloc]initWithFrame:CGRectMake(xPosition + (cardWidth/2 - buttonSize)/2, buttonYPosition, buttonSize, buttonSize)];
    [xButton setImage:[UIImage imageNamed:@"UnloveButton"] forState:UIControlStateNormal];
    [xButton addTarget:self action:@selector(swipeLeft) forControlEvents:UIControlEventTouchUpInside];
    checkButton = [[UIButton alloc]initWithFrame:CGRectMake(xPosition + cardWidth/2 + (cardWidth/2 - buttonSize)/2, buttonYPosition, buttonSize, buttonSize)];
    [checkButton setImage:[UIImage imageNamed:@"LoveButton"] forState:UIControlStateNormal];
    [checkButton addTarget:self action:@selector(swipeRight) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:menuButton];
    [self addSubview:messageButton];
    [self addSubview:xButton];
    [self addSubview:checkButton];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveCardForLater:) name:@"com.imhoapp.notificationsavemissionforlater" object:nil];
}


//%%% creates a card and returns it.  This should be customized to fit your needs.
// use "index" to indicate where the information should be pulled.  If this doesn't apply to you, feel free
// to get rid of it (eg: if you are building cards from data from the internet)
-(DraggableView *)createDraggableViewWithDataAtIndex:(NSInteger)index
{
    
    CGFloat cardWidth = self.frame.size.width*0.8;
    CGFloat cardHeight = self.frame.size.height*0.7;
    CGFloat xPosition = (self.frame.size.width - cardWidth)/2;
    CGFloat yPosition = (self.frame.size.height - cardHeight)/4;
    
    DraggableView *draggableView = [[DraggableView alloc]initWithFrame:CGRectMake(xPosition, yPosition, cardWidth, cardHeight) andImages:[[(MissionModel *)[missionArray objectAtIndex:index] images] count]];
    [draggableView setupMission:[missionArray objectAtIndex:index]];
    draggableView.delegate = self;
    return draggableView;
}

//%%% loads all the cards and puts the first x in the "loaded cards" array
-(void)loadCards
{
    if([missionArray count] > 0) {
        NSInteger numLoadedCardsCap =(([missionArray count] > MAX_BUFFER_SIZE)?MAX_BUFFER_SIZE:[missionArray count]);
        //%%% if the buffer size is greater than the data size, there will be an array error, so this makes sure that doesn't happen
        
        //%%% loops through the exampleCardsLabels array to create a card for each label.  This should be customized by removing "exampleCardLabels" with your own array of data
        for (int i = 0; i<[missionArray count]; i++) {
            DraggableView* newCard = [self createDraggableViewWithDataAtIndex:i];
            [allCards addObject:newCard];
            
            if (i<numLoadedCardsCap) {
                //%%% adds a small number of cards to be loaded
                [loadedCards addObject:newCard];
            }
        }
        
        //%%% displays the small number of loaded cards dictated by MAX_BUFFER_SIZE so that not all the cards
        // are showing at once and clogging a ton of data
        for (int i = 0; i<[loadedCards count]; i++) {
            if (i>0) {
                [self insertSubview:[loadedCards objectAtIndex:i] belowSubview:[loadedCards objectAtIndex:i-1]];
            } else {
                [self addSubview:[loadedCards objectAtIndex:i]];
            }
            cardsLoadedIndex++; //%%% we loaded a card into loaded cards, so we have to increment
        }
    }
    
    [self changeAnswerButtons];
}


//%%% action called when the card goes to the left.
// This should be customized with your own action
-(void)cardSwipedLeft:(UIView *)card;
{
    //do whatever you want with the card that was swiped
    //    DraggableView *c = (DraggableView *)card;
    
    [loadedCards removeObjectAtIndex:0]; //%%% card was swiped, so it's no longer a "loaded card"
    
    if (cardsLoadedIndex < [allCards count]) { //%%% if we haven't reached the end of all cards, put another into the loaded cards
        [loadedCards addObject:[allCards objectAtIndex:cardsLoadedIndex]];
        cardsLoadedIndex++;//%%% loaded a card, so have to increment count
        [self insertSubview:[loadedCards objectAtIndex:(MAX_BUFFER_SIZE-1)] belowSubview:[loadedCards objectAtIndex:(MAX_BUFFER_SIZE-2)]];
    }
    
    [self changeAnswerButtons];
}


//%%% action called when the card goes to the right.
// This should be customized with your own action
-(void)cardSwipedRight:(UIView *)card
{
    //do whatever you want with the card that was swiped
    //    DraggableView *c = (DraggableView *)card;
    
    [loadedCards removeObjectAtIndex:0]; //%%% card was swiped, so it's no longer a "loaded card"
    
    if (cardsLoadedIndex < [allCards count]) { //%%% if we haven't reached the end of all cards, put another into the loaded cards
        [loadedCards addObject:[allCards objectAtIndex:cardsLoadedIndex]];
        cardsLoadedIndex++;//%%% loaded a card, so have to increment count
        [self insertSubview:[loadedCards objectAtIndex:(MAX_BUFFER_SIZE-1)] belowSubview:[loadedCards objectAtIndex:(MAX_BUFFER_SIZE-2)]];
    }
    
    [self changeAnswerButtons];

}

//%%% when you hit the right button, this is called and substitutes the swipe
-(void)swipeRight
{
    DraggableView *dragView = [loadedCards firstObject];
    dragView.overlayView.mode = GGOverlayViewModeRight;
    [UIView animateWithDuration:0.2 animations:^{
        dragView.overlayView.alpha = 1;
    }];
    [dragView rightClickAction];
    
    [self changeAnswerButtons];
}

//%%% when you hit the left button, this is called and substitutes the swipe
-(void)swipeLeft
{
    DraggableView *dragView = [loadedCards firstObject];
    dragView.overlayView.mode = GGOverlayViewModeLeft;
    [UIView animateWithDuration:0.2 animations:^{
        dragView.overlayView.alpha = 1;
    }];
    [dragView leftClickAction];
    
    [self changeAnswerButtons];
}

-(void)saveCardForLater:(NSNotification *)notification {
 
    DraggableView *dragView = [loadedCards firstObject];
    [dragView saveForLater];
}

-(void)addCommentToLastCard:(NSString *)comment {
    
    DraggableView *lastCard = [loadedCards firstObject];
    [lastCard addComment:comment];
}

-(void)changeAnswerButtons {
    
    if ([loadedCards count] > 0) {
        
        [xButton setHidden:NO];
        [checkButton setHidden:NO];
        
        DraggableView *lastCard = [loadedCards firstObject];

        if ([lastCard isMissionVersus]) {
            
            [xButton setImage:[UIImage imageNamed:@"VersusAnswerLeft"] forState:UIControlStateNormal];
            [checkButton setImage:[UIImage imageNamed:@"VersusAnswerRight"] forState:UIControlStateNormal];
            
        } else {
            [xButton setImage:[UIImage imageNamed:@"UnloveButton"] forState:UIControlStateNormal];
            [checkButton setImage:[UIImage imageNamed:@"LoveButton"] forState:UIControlStateNormal];
        }
    } else {
        [xButton setHidden:YES];
        [checkButton setHidden:YES];
        
        AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
        
        UIColor *appMainColor = [UIColor colorWithRed:192/255.0 green:23/255.0 blue:68/255.0 alpha:1];
        emptyStateButton = [[UIButton alloc] initWithFrame:CGRectMake((self.frame.size.width/2)-100, (self.frame.size.height/2) - 20, 200, 40)];
        [emptyStateButton setTitle:[delegate getLocalizedStringForKey:@"createNewMission"] forState:UIControlStateNormal];
        [emptyStateButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:17]];
        [emptyStateButton.layer setBackgroundColor:[[UIColor clearColor] CGColor]];
        [emptyStateButton.layer setBorderColor:[appMainColor CGColor]];
        [emptyStateButton.layer setBorderWidth:1];
        [emptyStateButton setTitleColor:appMainColor forState:UIControlStateNormal];
        [emptyStateButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        [emptyStateButton.layer setCornerRadius:5];
        [emptyStateButton addTarget:self action:@selector(createMissionTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:emptyStateButton];
    }
}

-(void)createMissionTapped:(UIButton *)sender {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"com.imhoapp.es.notificationcreatemission" object:nil];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
