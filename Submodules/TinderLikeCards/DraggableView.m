//
//  DraggableView.m
//  testing swiping
//
//  Created by Richard Kim on 5/21/14.
//  Copyright (c) 2014 Richard Kim. All rights reserved.
//
//  @cwRichardKim for updates and requests

#define ACTION_MARGIN 120 //%%% distance from center where the action applies. Higher = swipe further in order for the action to be called
#define SCALE_STRENGTH 4 //%%% how quickly the card shrinks. Higher = slower shrinking
#define SCALE_MAX .93 //%%% upper bar for how much the card shrinks. Higher = shrinks less
#define ROTATION_MAX 1 //%%% the maximum rotation allowed in radians.  Higher = card can keep rotating longer
#define ROTATION_STRENGTH 320 //%%% strength of rotation. Higher = weaker rotation
#define ROTATION_ANGLE M_PI/8 //%%% Higher = stronger rotation angle


#import "DraggableView.h"
#import "IMHO-Swift.h"

@implementation DraggableView {
    CGFloat xFromCenter;
    CGFloat yFromCenter;
    UIImageView *imageView;
    UIImageView *secondImageView;
    UILabel *questionLabel;
    MissionModel *mission;
    NSString *commentToAdd;
}

//delegate is instance of ViewController
@synthesize delegate;

@synthesize panGestureRecognizer;
@synthesize information;
@synthesize overlayView;

- (id)initWithFrame:(CGRect)frame andImages:(NSInteger)images
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupView];
        
// placeholder stuff, replace with card-specific information {
        information = [[UILabel alloc]initWithFrame:CGRectMake(0, 50, self.frame.size.width, 100)];
        information.text = @"no info given";
        [information setTextAlignment:NSTextAlignmentCenter];
        information.textColor = [UIColor blackColor];
        
        self.backgroundColor = [UIColor whiteColor];

        if (images > 1) {
         
            imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width/2, self.frame.size.height*0.87)];
            secondImageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width/2, 0, self.frame.size.width/2, self.frame.size.height*0.87)];
            secondImageView.contentMode = UIViewContentModeScaleAspectFill;
            [secondImageView setClipsToBounds:YES];
            
            [self addSubview:secondImageView];
            
        } else {
            imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height*0.87)];
        }
        
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        [imageView setClipsToBounds:YES];
        
        UIImageView *filterImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, imageView.frame.size.height*0.2)];
        [filterImageView setBackgroundColor:[UIColor blackColor]];
        [filterImageView setAlpha:0.6];
        
        questionLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width*0.05, (filterImageView.frame.size.height-21)/2, self.frame.size.width, 21)];
        [questionLabel setTextColor:[UIColor whiteColor]];
        [questionLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:17]];
        [questionLabel setNumberOfLines:3];
        
        UIButton *forwardButton = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width/6 - 40, self.frame.size.height - 40, 40, 40)];
        [forwardButton setImage:[UIImage imageNamed:@"CardForwardButton"] forState:UIControlStateNormal];
        [forwardButton addTarget:self action:@selector(forwardTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *commentButton = [[UIButton alloc] initWithFrame:CGRectMake(3*self.frame.size.width/7 - 40, self.frame.size.height - 40, 40, 40)];
        [commentButton setImage:[UIImage imageNamed:@"CardCommentButton"] forState:UIControlStateNormal];
        [commentButton addTarget:self action:@selector(commentTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *shareButton = [[UIButton alloc] initWithFrame:CGRectMake((5*self.frame.size.width/7) - 40, self.frame.size.height - 42, 40, 40)];
        [shareButton setImage:[UIImage imageNamed:@"CardShareButton"] forState:UIControlStateNormal];
        [shareButton addTarget:self action:@selector(shareTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *moreButton = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width - 40, self.frame.size.height - 23, 29, 7)];
        [moreButton setImage:[UIImage imageNamed:@"CardMoreButton"] forState:UIControlStateNormal];
        [moreButton addTarget:self action:@selector(moreTapped:) forControlEvents:UIControlEventTouchUpInside];
        
// placeholder stuff, replace with card-specific information }
        
        panGestureRecognizer = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(beingDragged:)];
        
        [self addGestureRecognizer:panGestureRecognizer];
        [self addSubview:imageView];
        [self addSubview:filterImageView];
        [self addSubview:questionLabel];
        [self addSubview:forwardButton];
        [self addSubview:commentButton];
        [self addSubview:shareButton];
        [self addSubview:moreButton];
        
        if (images > 1) {
            UIImageView *separatorImageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width/2, 0, 1, imageView.frame.size.height)];
            [separatorImageView setBackgroundColor:[UIColor blackColor]];
            [self addSubview:separatorImageView];
        }
        
        overlayView = [[OverlayView alloc]initWithFrame:CGRectMake(self.frame.size.width/2-100, 0, 100, 100)];
        overlayView.alpha = 0;
        [self addSubview:overlayView];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didForwardMission:) name:@"com.imhoapp.notificationdidforwardmission" object:nil];
    }
    return self;
}

-(void)setupView
{
    self.layer.cornerRadius = 0;
    self.layer.shadowRadius = 0;
    self.layer.shadowOpacity = 0.1;
    self.layer.shadowOffset = CGSizeMake(1, 1);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

//%%% called when you move your finger across the screen.
// called many times a second
-(void)beingDragged:(UIPanGestureRecognizer *)gestureRecognizer
{
    //%%% this extracts the coordinate data from your swipe movement. (i.e. How much did you move?)
    xFromCenter = [gestureRecognizer translationInView:self].x; //%%% positive for right swipe, negative for left
    yFromCenter = [gestureRecognizer translationInView:self].y; //%%% positive for up, negative for down
    
    //%%% checks what state the gesture is in. (are you just starting, letting go, or in the middle of a swipe?)
    switch (gestureRecognizer.state) {
            //%%% just started swiping
        case UIGestureRecognizerStateBegan:{
            self.originalPoint = self.center;
            break;
        };
            //%%% in the middle of a swipe
        case UIGestureRecognizerStateChanged:{
            //%%% dictates rotation (see ROTATION_MAX and ROTATION_STRENGTH for details)
            CGFloat rotationStrength = MIN(xFromCenter / ROTATION_STRENGTH, ROTATION_MAX);
            
            //%%% degree change in radians
            CGFloat rotationAngel = (CGFloat) (ROTATION_ANGLE * rotationStrength);
            
            //%%% amount the height changes when you move the card up to a certain point
            CGFloat scale = MAX(1 - fabs(rotationStrength) / SCALE_STRENGTH, SCALE_MAX);
            
            //%%% move the object's center by center + gesture coordinate
            self.center = CGPointMake(self.originalPoint.x + xFromCenter, self.originalPoint.y + yFromCenter);
            
            //%%% rotate by certain amount
            CGAffineTransform transform = CGAffineTransformMakeRotation(rotationAngel);
            
            //%%% scale by certain amount
            CGAffineTransform scaleTransform = CGAffineTransformScale(transform, scale, scale);
            
            //%%% apply transformations
            self.transform = scaleTransform;
            [self updateOverlay:xFromCenter];
            
            break;
        };
            //%%% let go of the card
        case UIGestureRecognizerStateEnded: {
            [self afterSwipeAction];
            break;
        };
        case UIGestureRecognizerStatePossible:break;
        case UIGestureRecognizerStateCancelled:break;
        case UIGestureRecognizerStateFailed:break;
    }
}

//%%% checks to see if you are moving right or left and applies the correct overlay image
-(void)updateOverlay:(CGFloat)distance
{
    if (distance > 0) {
        overlayView.mode = GGOverlayViewModeRight;
    } else {
        overlayView.mode = GGOverlayViewModeLeft;
    }
    
    overlayView.alpha = 0;//MIN(fabs(distance)/100, 0.4);
}

//%%% called when the card is let go
- (void)afterSwipeAction
{
    if (xFromCenter > ACTION_MARGIN) {
        [self rightAction];
    } else if (xFromCenter < -ACTION_MARGIN) {
        [self leftAction];
    } else { //%%% resets the card
        [UIView animateWithDuration:0.3
                         animations:^{
                             self.center = self.originalPoint;
                             self.transform = CGAffineTransformMakeRotation(0);
                             overlayView.alpha = 0;
                         }];
    }
}

//%%% called when a swipe exceeds the ACTION_MARGIN to the right
-(void)rightAction
{
    CGPoint finishPoint = CGPointMake(500, 2*yFromCenter +self.originalPoint.y);
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.center = finishPoint;
                     }completion:^(BOOL complete){
                         [self removeFromSuperview];
                     }];
    
    [delegate cardSwipedRight:self];
    
    NSLog(@"YES");
    
    [self sendTrueAnswer];
    
}

//%%% called when a swip exceeds the ACTION_MARGIN to the left
-(void)leftAction
{
    CGPoint finishPoint = CGPointMake(-500, 2*yFromCenter +self.originalPoint.y);
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.center = finishPoint;
                     }completion:^(BOOL complete){
                         [self removeFromSuperview];
                     }];
    
    [delegate cardSwipedLeft:self];
    
    NSLog(@"NO");
    
    [self sendFalseAnswer];
}

-(void)rightClickAction
{
    CGPoint finishPoint = CGPointMake(600, self.center.y);
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.center = finishPoint;
                         self.transform = CGAffineTransformMakeRotation(1);
                     }completion:^(BOOL complete){
                         [self removeFromSuperview];
                     }];
    
    [delegate cardSwipedRight:self];
    
    NSLog(@"YES");
    
    [self sendTrueAnswer];
}

-(void)leftClickAction
{
    CGPoint finishPoint = CGPointMake(-600, self.center.y);
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.center = finishPoint;
                         self.transform = CGAffineTransformMakeRotation(-1);
                     }completion:^(BOOL complete){
                         [self removeFromSuperview];
                     }];
    
    [delegate cardSwipedLeft:self];
    
    NSLog(@"NO");
    
    [self sendFalseAnswer];
}

- (void)sendTrueAnswer {
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    AnswerModel *answer = [[AnswerModel alloc] initWithAUser:[appDelegate loggedUser] aMission:mission aComment:(commentToAdd != nil)? commentToAdd:@"" aImage:[mission.images objectAtIndex:0] aSingleAnswer:YES aPoints:10];
    
    [answer saveInBackgroundWithSuccess:^(AnswerModel *savedAnswer) {
        
        __block MissionModel *weakMission = mission;
        
        [mission addAnswer:savedAnswer withSuccess:^(BOOL succeeded) {
            if (succeeded) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"com.imho.app.notificationanswerdidpost" object:nil userInfo:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:weakMission, [NSNumber numberWithBool:YES], nil] forKeys:[NSArray arrayWithObjects:@"mission", @"answer", nil]]];
               
            //Calling the method that saves the answer to the mision
            [appDelegate.loggedUser saveInformationWithSuccess:^{
                
            } errorBlock:^(NSError *error) {
                NSLog(@"Error al guardar el usuario");
            }];
                
            //Calling the method that adds 2 points to the user who answered a mission.
                
            [appDelegate.loggedUser saveUserPointsWithSuccess:^{
                    
            } errorBlock:^(NSError *error) {
                    NSLog(@"Error al guardar los puntos del usuario");
            }];
                
            //Calling the method that checks if the user answer is the last one to start the closing mission mechanism.
                [mission checkInCloudIfLastAnswer:^{
                    //successblock
                } errorBlock:^(NSError *error) {
                    //error clobk
                    NSLog(@"Error al comprobar si la respuesta del usuario es la ultima.");
                }];
                
            
             }else {
                
            }
        }];
        
    }];
}

-(void)sendFalseAnswer {
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    AnswerModel *answer = [[AnswerModel alloc] initWithAUser:[appDelegate loggedUser] aMission:mission aComment:(commentToAdd != nil)? commentToAdd:@"" aImage:[mission.images objectAtIndex:0] aSingleAnswer:NO aPoints:10];
    
    [answer saveInBackgroundWithSuccess:^(AnswerModel *savedAnswer) {
        
        __block MissionModel *weakMission = mission;
        
        [mission addAnswer:savedAnswer withSuccess:^(BOOL succeeded) {
            if (succeeded) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"com.imho.app.notificationanswerdidpost" object:nil userInfo:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:weakMission, [NSNumber numberWithBool:NO], nil] forKeys:[NSArray arrayWithObjects:@"mission",@"answer",nil]]];
        /*
         ADDED BY XAVI
         */
         
            
                //Calling the method that saves the answer to the mision
                [appDelegate.loggedUser saveInformationWithSuccess:^{
                    
                } errorBlock:^(NSError *error) {
                    NSLog(@"Error al guardar el usuario");
                }];
                
                //Calling the method that adds 2 points to the user who answered a mission.
                
                [appDelegate.loggedUser saveUserPointsWithSuccess:^{
                    
                } errorBlock:^(NSError *error) {
                    NSLog(@"Error al guardar los puntos del usuario");
                }];
                
                
            } else {
                
            }
        }];
        
    }];
}

-(void)setupMission:(id)aMission {
    
    mission = (MissionModel *) aMission;
    
    if (mission.images.count == 1) {
    
        for (ImageModel *image in mission.images) {
            
            [image getImageWithBlock:^(UIImage * result) {
                imageView.image = result;
            }];
            
        }
    } else {
    
        ImageModel *image = [mission.images objectAtIndex:0];
        [image getImageWithBlock:^(UIImage * result) {
            imageView.image = result;
        }];
        
        ImageModel *image2 = [mission.images objectAtIndex:1];
        [image2 getImageWithBlock:^(UIImage * result) {
            secondImageView.image = result;
        }];
    }
    
    
    [questionLabel setText:[mission question]];
    
}

-(void)forwardTapped:(UIButton *)sender {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"com.imhoapp.notificationforwardtapped" object:nil userInfo:[NSDictionary dictionaryWithObject:mission forKey:@"mission"]];
}

-(void)shareTapped:(UIButton *)sender {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"com.imhoapp.notificationsharetapped" object:nil userInfo:[NSDictionary dictionaryWithObject:mission forKey:@"mission"]];
}

-(void)commentTapped:(UIButton *)sender {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"com.imhoapp.didselectcomment" object:nil userInfo:[NSDictionary dictionaryWithObject:mission forKey:@"mission"]];
}

-(void)moreTapped:(UIButton *)sender {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"com.imhoapp.notificationmoretapped" object:nil userInfo:[NSDictionary dictionaryWithObject:mission forKey:@"mission"]];
}

-(void)didForwardMission:(NSNotification *)notification {
    
    
    if ([mission.identifier isEqualToString:[(MissionModel *)[notification.userInfo objectForKey:@"mission"] identifier]]) {
        
        mission.singleUsers = [[(MissionModel *)[notification.userInfo objectForKey:@"mission"] singleUsers] mutableCopy];
    }
}

-(void)saveForLater {
    
    CGPoint finishPoint = CGPointMake(600, self.center.y);
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.center = finishPoint;
                         self.transform = CGAffineTransformMakeRotation(1);
                     }completion:^(BOOL complete){
                         [self removeFromSuperview];
                     }];
    
    [delegate cardSwipedRight:self];
}

-(void)addComment:(NSString *)comment {
    
    commentToAdd = comment;
}

-(BOOL)isMissionVersus {
    
    return mission.images.count > 1;
}

@end
