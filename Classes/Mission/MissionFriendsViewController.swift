//
//  MissionFriendsViewController.swift
//  IMHO
//
//  Created by Rafael Zamora on 5/18/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import UIKit

class MissionFriendsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    var delegate:AppDelegate = (UIApplication.sharedApplication().delegate! as! AppDelegate)
    var friends:NSMutableArray = NSMutableArray()
    
    var indexArray:NSMutableArray = NSMutableArray()
    @IBOutlet weak var friendsTableView: UITableView!
    
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Set screen title
        self.title = getLocalizedStringForKey("addFriendsTitle")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        setupView()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - MissionFriendsViewController methods
    
    func setupView() {
        
        if (self.friends.count == 0) {
            self.doneButton.enabled = false
        }
        
        for (var i:Int = 0; i < friends.count; i++) {
            
            let userAdded:UserModel = friends.objectAtIndex(i) as! UserModel
            
            var added:Bool = false
            
            for friend in delegate.loggedUser.friends! {
                
                if ((friend as! UserModel).identifier! == userAdded.identifier!) {
                    added = true
                }
            }
            
            if (added) {
                self.indexArray.addObject(i)
            }
        }
        
        self.friendsTableView.reloadData()
    }

    // MARK: - MissionFriendsViewController actions
    
    @IBAction func doneTapped(sender: AnyObject) {
        
        NSNotificationCenter.defaultCenter().postNotificationName(kNotificationDidAddUsersToMission, object: nil, userInfo: NSDictionary(object: self.friends, forKey: "friends") as [NSObject : AnyObject])
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func cancelTapped(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - UITableViewDataSource methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return delegate.loggedUser.friends!.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:FriendsTableViewCell = tableView.dequeueReusableCellWithIdentifier("MissionFriendCell") as! FriendsTableViewCell
        
        let friend:UserModel = delegate.loggedUser.friends!.objectAtIndex(indexPath.row) as! UserModel
        
        cell.FriendsTitleLabel.text = friend.name!
        
        friend.getProfileImageWithSuccess({ (image) -> () in
            
            cell.FriendsImageView.image = image
            
            }, errorBlock: { (error) -> () in
                
        })
        
        if (indexArray.containsObject(indexPath.row)) {
            cell.checkImageView.hidden = false
        } else {
            cell.checkImageView.hidden = true
        }
        
        delegate.getRoundImageFromImage(cell.FriendsImageView)
        
        return cell
    }
    
    // MARK: - UITableViewDelegate methods
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if (self.indexArray.containsObject(indexPath.item)) {
            self.indexArray.removeObject(indexPath.item)
            self.friends.removeObject(delegate.loggedUser.friends!.objectAtIndex(indexPath.row))
        } else {
            self.indexArray.addObject(indexPath.item)
            self.friends.addObject(delegate.loggedUser.friends!.objectAtIndex(indexPath.row))
        }
        
        if (self.indexArray.count > 0) {
            self.doneButton.enabled = true
        } else {
            self.doneButton.enabled = false
        }
        
        self.friendsTableView.reloadData()
        
    }
}
