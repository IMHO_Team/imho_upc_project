//
//  TimeCollectionViewCell.swift
//  IMHO
//
//  Created by Andrea Aranguren on 5/16/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import UIKit


class TimeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var timeButton: UIButton!
    @IBOutlet weak var containerView: MissionContainerView!
    
    
    func setData(array:NSMutableArray) {
        
        containerView.setupData(array)
    }
}
