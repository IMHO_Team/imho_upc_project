//
//  MissionContainerView.swift
//  IMHO
//
//  Created by Rafael Zamora on 5/17/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import UIKit

class MissionContainerView: UIView,UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet weak var friendsCollectionView:UICollectionView!
    var delegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    var data:NSMutableArray = NSMutableArray()
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    
    func setupData(array:NSMutableArray) {
        
        self.data = array.mutableCopy() as! NSMutableArray
        self.friendsCollectionView.reloadData()
    }

    // MARK: - UICollectionViewDataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count+1;
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell:TimeCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("FriendCell", forIndexPath: indexPath) as! TimeCollectionViewCell
        
        if (indexPath.item == 0) {
            
            cell.timeButton.setBackgroundImage(UIImage(named: "MissionAddFriend"), forState: UIControlState.Normal)
            cell.timeButton.setBackgroundImage(UIImage(named: "MissionAddFriend"), forState: UIControlState.Highlighted)
            
        } else {
         
            let friend:UserModel = self.data.objectAtIndex(indexPath.item - 1) as! UserModel
            
            /*cell.timeButton.setTitle("\(durationsArray.objectAtIndex(indexPath.item))" + delegate.getLocalizedStringForKey("days") , forState:UIControlState.Highlighted)
            cell.timeButton.setTitle("\(durationsArray.objectAtIndex(indexPath.item))" + delegate.getLocalizedStringForKey("days"), forState:UIControlState.Normal)
            cell.timeButton.setTitleColor(kAppSecondaryColor, forState: .Normal)*/
            
            friend.getProfileImageWithSuccess({ (image) -> () in
                
                cell.timeButton.setBackgroundImage(image, forState: UIControlState.Normal)
                cell.timeButton.setBackgroundImage(image, forState: UIControlState.Highlighted)
                
                }, errorBlock: { (error) -> () in
                    
            })
            
            cell.timeButton.backgroundColor = UIColor.clearColor()
            cell.timeButton.layer.cornerRadius = 0.5 * cell.timeButton.bounds.size.width
            cell.timeButton.layer.borderColor = kAppSecondaryColor.CGColor
            cell.timeButton.layer.borderWidth = 1.0
            /*if(indexPath.item == timeSelectedIndex){
            cell.timeButton.backgroundColor = kAppSecondaryColor
            cell.timeButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            }*/
        }
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        if (indexPath.item == 0) {
            
            //NSNotificationCenter.defaultCenter().postNotificationName(kNotificationDidSelectAddFriend, object: nil)
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(collectionView.frame.size.width/5, collectionView.frame.size.width/5)
    }
    
}
