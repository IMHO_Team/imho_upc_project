
//
//  MissionViewController.swift
//  IMHO
//
//  Created by Andrea Aranguren on 3/23/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import UIKit

class MissionViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, SegmentedButtonDelegate, UIActionSheetDelegate, UICollectionViewDelegate, UITextFieldDelegate {
    
    
    //Variables
    @IBOutlet weak var questionTextField: UITextField!
    @IBOutlet weak var privateMissionOptionsContainer: UIView!
    @IBOutlet weak var segmentedButton: SegmentedButton!
    @IBOutlet weak var firstImageView: UIImageView!
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var secondImageView: UIImageView!
    @IBOutlet weak var categoryListContainer: UIView!
    @IBOutlet weak var deleteImage1Button: UIButton!
    @IBOutlet weak var deleteImage2Button: UIButton!
    @IBOutlet weak var filterImageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var timeSelectorCollectionView: UICollectionView!
    var categoryList: NSMutableArray = NSMutableArray()
    let picker = UIImagePickerController()
    var delegate:AppDelegate = (UIApplication.sharedApplication().delegate! as! AppDelegate)
    //Images initialization
    var firstImageWasAdded = false
    var secondImageWasAdded = false
    var firstImageFileName:String = "image1"
    var secondImageFileName:String = "image2"
    //Category and time initialization
    var categoryIndexSelected = -1
    var timeSelectedIndex = 2
    var durationsArray: NSMutableArray = NSMutableArray(array: [1, 2, 3, 5, 7])
    
    var selectedFriends:NSMutableArray = NSMutableArray()
    
    var isModal:Bool = false
    
    let kDefaultFilterAlpha:CGFloat = 0.5
    
    //Functions
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loadImageViewListener()
        picker.delegate = self
        categoryListContainer.hidden = true
        secondImageView.tag = 2
        
        self.questionTextField.delegate = self;
        
        
        
        sendButton.backgroundColor = kAppMainColor
        segmentedButton.setupview(NSArray(objects: getLocalizedStringForKey("newMissionPrivateButton"), getLocalizedStringForKey("newMissionPublicButton")))
        segmentedButton.delegate = self
        
        questionTextField.placeholder = getLocalizedStringForKey("askSomething")
        loadCategories()
        
        registerNotifications()
        
        // Hide loading at the beginning
        self.filterImageView.alpha = 0
        self.activityIndicator.alpha = 0
        self.filterImageView.hidden = true
        self.activityIndicator.hidden = true
        
    }
    
    //MARK: - MissionController Methods
    
    /*Method to close the keyboard afetr return pressed.
    *
    */
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    
    @IBAction func sendMissionButtonClicked(sender: UIButton) {
        // IMPORTANTE En version final comentar esto
        //addMissionToParse()
        
        // IMPORTANTE En version final descomentar esto
        uploadMission()
    }
    
    func addMissionToParse(){
        //IsSecret boolean set to false always. pending new version
        
        let isSecret = false
        
        var question = questionTextField.text;
        
        let images = NSMutableArray()
        if(firstImageWasAdded){
            
            images.addObject(ImageModel(aFileName: firstImageFileName))
            
            
        }
        if(secondImageWasAdded){
           
            images.addObject(ImageModel(aFileName: secondImageFileName))
            if(question!.isEmpty){
                question = getLocalizedStringForKey("whichDoYouPrefer")
            }
        }else{
            if(question!.isEmpty){
                question = getLocalizedStringForKey("doYouLikeIt")
            }
        }
        var isPrivate = true;
        if(segmentedButton.selectedIndex == 1){
            isPrivate = false
        }
        
        
        var missionToSave = MissionModel(aQuestion: question!, aUser: delegate.loggedUser, anImages: images, isPrivate: isPrivate, isSecret: isSecret,  aStatus: kParseValueMissionStatusRunning, aLength: durationsArray.objectAtIndex(2) as! Int, users: self.selectedFriends)
        
        if(!isPrivate){
            let category:CategoryModel = categoryList.objectAtIndex(categoryIndexSelected) as! CategoryModel
            missionToSave = MissionModel(aQuestion: question!, aUser: delegate.loggedUser, anImages: images, aCategory: category , isPrivate: isPrivate, isSecret: isSecret,  aStatus: kParseValueMissionStatusRunning, aLength: durationsArray.objectAtIndex(2) as! Int, users: NSMutableArray())
        }else{
            
            missionToSave = MissionModel(aQuestion: question!, aUser: delegate.loggedUser, anImages: images, isPrivate: isPrivate, isSecret: isSecret,  aStatus: kParseValueMissionStatusRunning, aLength: durationsArray.objectAtIndex(timeSelectedIndex) as! Int, users: self.selectedFriends)
            
        }
        
  
            //uploadMission()
            DataSource.sharedInstance.addMission(missionToSave, withSuccessBlock: {() -> () in
                print("added")
                
                //Alert to the user to inform the mission creation
                let buttonPressedAlert:UIAlertView = UIAlertView(title: getLocalizedStringForKey("newMission"), message: getLocalizedStringForKey("missionSent"), delegate: nil, cancelButtonTitle: "OK")
                buttonPressedAlert.show()
                
                
                for user in self.selectedFriends {
                    DataSource.sharedInstance.notifyNewMissionToUser(user as! UserModel)
                }
                
                self.delegate.loggedUser.getCurrentMissions { (missions) -> () in
                    
                }
                
                self.clearScreen()
                
                }, andErrorBlock: {(error:NSError) -> () in print("not added")})
        
       
    }
    
    func registerNotifications() {
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "didAddUsersToMission:", name: kNotificationDidAddUsersToMission, object: nil)
    }
    
    func clearScreen(){
        
        self.stopLoading()
        
        if (isModal) {
            
            self.dismissViewControllerAnimated(true, completion: nil)
            
        } else {
            self.questionTextField.text = ""
            let defaultImage = UIImage(named: "RollInRectangle")
            self.firstImageView.image = defaultImage
            self.secondImageView.image = defaultImage
            self.selectedFriends = NSMutableArray()
            self.firstImageWasAdded = false
            self.secondImageWasAdded = false
            self.firstImageFileName = ""
            self.secondImageFileName = ""
            self.timeSelectedIndex = -1
            self.categoryIndexSelected = -1
            self.categoryCollectionView.reloadData()
            self.timeSelectorCollectionView.reloadData()
            self.deleteImage1Button.hidden = true
            self.deleteImage2Button.hidden = true
        }
    }
    
    func startLoading() {
        
        self.activityIndicator.startAnimating()
        
        self.filterImageView.hidden = false
        self.activityIndicator.hidden = false
        
        UIView.animateWithDuration(1.0, animations: { () -> Void in
            self.filterImageView.alpha = self.kDefaultFilterAlpha
            self.activityIndicator.alpha = 1
        })
    }
    
    func stopLoading() {
        
        self.activityIndicator.stopAnimating()
        
        UIView.animateWithDuration(1.0, animations: { () -> Void in
            self.filterImageView.alpha = 0
            self.activityIndicator.alpha = 0
            
            self.filterImageView.hidden = true
            self.activityIndicator.hidden = true
        })
        
    }
    
    func didAddUsersToMission(notification:NSNotification) {
        
        let friends: AnyObject? = (notification.userInfo! as NSDictionary).objectForKey("friends")
        
        self.selectedFriends = friends!.mutableCopy() as! NSMutableArray
        
        self.timeSelectorCollectionView.reloadData()
    }
    
    func getCurrentTime() -> String {
        let date = NSDate()
        let formatter = NSDateFormatter()
        formatter.dateFormat = "MMM_dd_yyyy_HH_mm_ss"
        let stringValue = formatter.stringFromDate(date)
        return stringValue
    }
    
    func uploadMission(){
        if(segmentedButton.selectedIndex == 0 && self.selectedFriends.count < 1){
         
            let buttonPressedAlert:UIAlertView = UIAlertView(title: getLocalizedStringForKey("error"), message: getLocalizedStringForKey("errorMissingFriends"), delegate: nil, cancelButtonTitle: "OK")
            buttonPressedAlert.show()
        } else{
        
        
        if(firstImageWasAdded ){

            self.startLoading()
            
            //Disable tabbar items while loading mission
            self.tabBarController?.hidesBottomBarWhenPushed = true
            
            let transferManager = AWSS3TransferManager.defaultS3TransferManager()
            firstImageFileName = delegate.loggedUser.identifier! + "1" + getCurrentTime() + ".jpg"
            
            
            let uploadFilePath:String = NSTemporaryDirectory().stringByAppendingPathComponent(firstImageFileName)
            let uploadFileURL:NSURL = NSURL(fileURLWithPath: uploadFilePath)
 
            let uploadRequest:AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
            //140
            
            //Image uploaded size same as the image taken from camera or camera roll. Compression set to 0.6
            
            let newSize:CGSize = CGSizeMake(self.firstImageView.image!.size.width/6, self.firstImageView.image!.size.height/6)
            UIGraphicsBeginImageContext(newSize)

            firstImageView.image!.drawInRect(CGRectMake(0, 0, newSize.width, newSize.height))
            let resizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            let data = UIImageJPEGRepresentation(resizedImage, 0.6)
            data!.writeToURL(uploadFileURL, atomically: true)
            uploadRequest.bucket = kS3BucketName
            uploadRequest.key =  firstImageFileName
            uploadRequest.body = uploadFileURL
            uploadRequest.ACL = AWSS3ObjectCannedACL.PublicRead
            uploadRequest.contentType = kDefaultContentType
            
            let task = transferManager.upload(uploadRequest)
            task.continueWithBlock { (task) -> AnyObject! in
                if task.error != nil {
                    print("Error: \(task.error)")
                    self.showUploadError()
                } else {
                    print("Upload successful")
                    if(self.secondImageWasAdded){
                        // TODO CODIGO Chapu CAMBIAR luego EXTRAER METODO
                        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                        self.secondImageFileName = self.delegate.loggedUser.identifier! + "2" + self.getCurrentTime() + ".jpg"
                        let uploadFilePath:String = NSTemporaryDirectory().stringByAppendingPathComponent(self.secondImageFileName)
                        let uploadFileURL:NSURL = NSURL(fileURLWithPath: uploadFilePath)
                        
                        let uploadRequest:AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
                        
                        let newSize2:CGSize = CGSizeMake(self.secondImageView.image!.size.width/6, self.secondImageView.image!.size.height/6)
                        UIGraphicsBeginImageContext(newSize2)
                        self.secondImageView.image!.drawInRect(CGRectMake(0, 0, newSize2.width, newSize2.height))
                        let resizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()
                        UIGraphicsEndImageContext()
                        
                        let data = UIImageJPEGRepresentation(resizedImage, 0.6)
                        data!.writeToURL(uploadFileURL, atomically: true)
                        uploadRequest.bucket = kS3BucketName
                        uploadRequest.key =  self.secondImageFileName
                        uploadRequest.body = uploadFileURL
                        uploadRequest.ACL = AWSS3ObjectCannedACL.PublicRead
                        uploadRequest.contentType = kDefaultContentType
                        
                        let task = transferManager.upload(uploadRequest)
                        task.continueWithBlock { (task) -> AnyObject! in
                            if task.error != nil {
                                print("Error: \(task.error)")
                            } else {
                                print("Upload successful")
                                self.addMissionToParse()
                            }
                            return nil
                        }
                    }else{
                        self.addMissionToParse()
                    }
                }
                return nil
            }
        }else{
            let buttonPressedAlert:UIAlertView = UIAlertView(title: getLocalizedStringForKey("error"), message: getLocalizedStringForKey("errorMissingImage"), delegate: nil, cancelButtonTitle: "OK")
            buttonPressedAlert.show()

        }
        }
        
    }
    
    func showUploadError(){
        let buttonPressedAlert:UIAlertView = UIAlertView(title: getLocalizedStringForKey("error"), message: getLocalizedStringForKey("errorSendingMission"), delegate: nil, cancelButtonTitle: "OK")
        buttonPressedAlert.show()

    }
    
    func loadCategories(){
        
        delegate.getCategoryListWithSuccessBlock { (categories) -> () in
            self.categoryList = categories.mutableCopy() as! NSMutableArray
           
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    @IBAction func addFriendTapped(sender: AnyObject) {
        
        self.performSegueWithIdentifier("missionFriends", sender: nil)
    }
    
    @IBAction func cancelTapped(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    @IBAction func deleteImage1Tapped(sender: AnyObject) {
        
        self.firstImageView.image = UIImage(named: "RollInRectangle")
        self.firstImageWasAdded = false
        self.deleteImage1Button.hidden = true
    }
    
    @IBAction func deleteImage2Tapped(sender: AnyObject) {
        
        self.secondImageView.image = UIImage(named: "RollInRectangle")
        self.secondImageWasAdded = false
        self.deleteImage2Button.hidden = true
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if (segue.identifier! == "missionFriends") {
            (((segue.destinationViewController as! UINavigationController).viewControllers as NSArray).firstObject as! MissionFriendsViewController).friends = self.selectedFriends
        }
        
    }

    
    // MARK: - Other methods
    
    func loadImageViewListener() {
        //instance the UITapGestureRecognizer and inform the method for the action "imageTapped"
        
        let tap = UITapGestureRecognizer(target: self, action: "askForPhotoSelector:")
        //define quantity of taps
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        //set the image to the gesture
        firstImageView.addGestureRecognizer(tap)
        let tap2 = UITapGestureRecognizer(target: self, action: "askForPhotoSelector:")
        //define quantity of taps
        tap2.numberOfTapsRequired = 1
        tap2.numberOfTouchesRequired = 1
        secondImageView.addGestureRecognizer(tap2)
        
    }
    
    func askForPhotoSelector(gesture: UIGestureRecognizer ){
        
        self.view.endEditing(true)
        let myActionSheet = UIActionSheet()
        myActionSheet.addButtonWithTitle(getLocalizedStringForKey("selectCameraOption"))
        myActionSheet.addButtonWithTitle(getLocalizedStringForKey("selectGalleryOption"))
        myActionSheet.addButtonWithTitle(getLocalizedStringForKey("cancelButton"))
        myActionSheet.cancelButtonIndex = 2
        myActionSheet.showInView(self.view)
        myActionSheet.delegate = self
        
    }
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        if(buttonIndex == 0){
            showCamera()
        }else if (buttonIndex == 1){
            showPictures()
        }
    }
    
    func actionSheetCancel(actionSheet: UIActionSheet) {
        
    }
    
    func showCamera(){
        if UIImagePickerController.availableCaptureModesForCameraDevice(.Rear) != nil {
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerControllerSourceType.Camera
            picker.cameraCaptureMode = .Photo
            presentViewController(picker, animated: true, completion: nil)
        } else {
            noCamera()
        }
        
        
    }
    
    func noCamera(){
        let alertVC = UIAlertController(title: "No Camera", message: "Sorry, this device has no camera", preferredStyle: .Alert)
        let okAction = UIAlertAction(title: "OK", style:.Default, handler: nil)
        alertVC.addAction(okAction)
        presentViewController(alertVC, animated: true, completion: nil)
    }
    
    func showPictures(){
        
        picker.allowsEditing = false
        picker.sourceType = .PhotoLibrary
        presentViewController(picker, animated: true, completion: nil)
        
        
    }
    
    //MARK: Delegates
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        if(firstImageWasAdded){
            secondImageView.contentMode = UIViewContentMode.ScaleAspectFill
            secondImageView.image = chosenImage
            secondImageWasAdded = true;
            deleteImage2Button.hidden = false
        }else{
            firstImageView.contentMode = UIViewContentMode.ScaleAspectFill
            firstImageView.image = chosenImage
            firstImageWasAdded = true;
            deleteImage1Button.hidden = false
        }
        //Define the prefs for imageview objects
        firstImageView.layer.masksToBounds = true
        secondImageView.layer.masksToBounds = true
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: UICollectionViewDataSource    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //#warning Incomplete method implementation -- Return the number of items in the section
        if(collectionView == categoryCollectionView){
            return categoryList.count
        }else{
            return durationsArray.count + 1
        }
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        if(collectionView == categoryCollectionView){
            let cell: PublicMissionCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("PublicMissionCategoryButton", forIndexPath: indexPath) as! PublicMissionCollectionViewCell
            cell.publicMissionCategoryButton.setTitle(categoryList.objectAtIndex(indexPath.item).name, forState:UIControlState.Normal)
            cell.publicMissionCategoryButton.setTitle(categoryList.objectAtIndex(indexPath.item).name, forState:UIControlState.Highlighted)
            cell.publicMissionCategoryButton.layer.cornerRadius = 14
            cell.publicMissionCategoryButton.layer.borderColor = kAppSecondaryColor.CGColor
            cell.publicMissionCategoryButton.layer.borderWidth = 1.0
            cell.publicMissionCategoryButton.backgroundColor = UIColor.clearColor()
            
            cell.publicMissionCategoryButton.setTitleColor(kAppSecondaryColor, forState: .Normal)
            if(indexPath.item == categoryIndexSelected){
                cell.publicMissionCategoryButton.backgroundColor = kAppSecondaryColor
                cell.publicMissionCategoryButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            }
            return cell
        }else if(collectionView == timeSelectorCollectionView){
            
            if (indexPath.item < self.durationsArray.count) {
                let cell: TimeCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("TimeButton", forIndexPath: indexPath) as! TimeCollectionViewCell
            
                cell.timeButton.setTitle("\(durationsArray.objectAtIndex(indexPath.item))" + getLocalizedStringForKey("days") , forState:UIControlState.Highlighted)
                cell.timeButton.setTitle("\(durationsArray.objectAtIndex(indexPath.item))" + getLocalizedStringForKey("days"), forState:UIControlState.Normal)
                cell.timeButton.setTitleColor(kAppSecondaryColor, forState: .Normal)
                cell.timeButton.backgroundColor = UIColor.clearColor()
                cell.timeButton.layer.cornerRadius = 0.5 * cell.timeButton.bounds.size.width
                cell.timeButton.layer.borderColor = kAppSecondaryColor.CGColor
                cell.timeButton.layer.borderWidth = 1.0
                if(indexPath.item == timeSelectedIndex){
                    cell.timeButton.backgroundColor = kAppSecondaryColor
                    cell.timeButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
                }
            
                return cell
            } else {
                let cell: TimeCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("FriendsCell", forIndexPath: indexPath) as! TimeCollectionViewCell
                
                cell.setData(self.selectedFriends)
                
                return cell
            }
        }else  {
            let cell: PublicMissionCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("PublicMissionCategoryButton", forIndexPath: indexPath) as! PublicMissionCollectionViewCell
            return cell
        }
    
        
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if(collectionView == categoryCollectionView){
            categoryIndexSelected = indexPath.item
            categoryCollectionView.reloadData()
        }else{
            timeSelectedIndex = indexPath.item
            timeSelectorCollectionView.reloadData()
        }
        
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        if(collectionView == categoryCollectionView){
            return CGSizeMake(self.view.frame.size.width/3, self.view.frame.size.width/3)
        }else{
            if (indexPath.item < self.durationsArray.count) {
                return CGSizeMake(collectionView.frame.size.width/5, collectionView.frame.size.width/5)
            } else {
                return CGSizeMake(collectionView.frame.size.width, collectionView.frame.size.width/5)
            }
        }
        
    }
    
    // MARK: - SegmentedButtonDelegate methods
    
    func didSelectButtonAtIndex(index: Int) {
        
        if (index == 0){
            self.categoryListContainer.hidden = true
            self.privateMissionOptionsContainer.hidden = false
        }else{
            self.categoryListContainer.hidden = false
            self.privateMissionOptionsContainer.hidden = true
            
            //loadCategories()
        }
        
    }


}
