//
//  RegisterViewController.swift
//  IMHO
//
//  Created by Andrea Aranguren on 3/17/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet weak var passwordBoxView: UIView!
    @IBOutlet weak var emailAddressBoxView: UIView!
    @IBOutlet weak var fullNameBoxView: UIView!

    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailAddressTextField: UITextField!
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var signUpButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    // MARK: RegisterViewController methods
    
    func setupView() {
        
        self.signUpButton.layer.cornerRadius = 20
        self.signUpButton.layer.borderColor = UIColor.whiteColor().CGColor
        self.signUpButton.layer.borderWidth = 2.0
        self.signUpButton.backgroundColor = UIColor.clearColor()
        
        self.fullNameBoxView.layer.cornerRadius = 20
        self.fullNameBoxView.layer.borderColor = UIColor.whiteColor().CGColor
        self.fullNameBoxView.layer.borderWidth = 1.0
        self.emailAddressBoxView.layer.cornerRadius = 20
        self.emailAddressBoxView.layer.borderColor = UIColor.whiteColor().CGColor
        self.emailAddressBoxView.layer.borderWidth = 1.0
        self.passwordBoxView.layer.cornerRadius = 20
        self.passwordBoxView.layer.borderColor = UIColor.whiteColor().CGColor
        self.passwordBoxView.layer.borderWidth = 1.0

    }

}
