//
//  FriendsViewController.swift
//  IMHO
//
//  Created by MacBook Pro on 15/3/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import UIKit

class FriendsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, SegmentedButtonDelegate  {
    
    
    @IBOutlet var segmentedButton: SegmentedButton!
    
    @IBOutlet var friendsTableView: UITableView!
    @IBOutlet var groupsTableView: UITableView!
    
    var delegate:AppDelegate = (UIApplication.sharedApplication().delegate! as! AppDelegate)

    //contador
    
    var friendsArray:NSMutableArray = NSMutableArray()
    var groupsArray:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.segmentedButton.setupview(NSArray(objects: getLocalizedStringForKey("homeFriendsTabButton"), "Groups"))
        self.segmentedButton.delegate = self
        
        self.groupsTableView.hidden = true
        
        self.title = getLocalizedStringForKey("friendsScreenTitle")
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if (segue.identifier == "friendsGroupSegue") {
            let group:GroupModel = sender as! GroupModel
            print("\(group.groupName!)")
        }
    }
    
    
    // Estos son los 3 metodos obligatorios para programar una tabla
    
    // Numero de secciones en la tabla (letras del abecedario)
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    // Numero de filas en cada seccion
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView == friendsTableView) {
            return delegate.loggedUser.friends!.count
        }
        else {
            return groupsArray.count
        }
    }
    // Vista de la celda
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell:FriendsTableViewCell;
        
        if (tableView == friendsTableView) {
            cell = tableView.dequeueReusableCellWithIdentifier("FriendsTableCell") as! FriendsTableViewCell
            
            let user:UserModel = delegate.loggedUser.friends!.objectAtIndex(indexPath.row) as! UserModel
            
            cell.FriendsTitleLabel.text = user.name!
            
            user.getProfileImageWithSuccess({ (image) -> () in
                
                cell.FriendsImageView.image = image
                
                }, errorBlock: { (error) -> () in
                    
            })
        }
        else {
            cell = tableView.dequeueReusableCellWithIdentifier("GroupsTableCell") as! FriendsTableViewCell
            let group:GroupModel = self.groupsArray.objectAtIndex(indexPath.row) as! GroupModel
            
            cell.FriendsTitleLabel.text = group.groupName!
        }

        cell.FriendsTitleLabel.font = helveticaNeueLightWithSize(16)
        cell.FriendsTitleLabel.textColor = kAppTextColor
        
        delegate.getRoundImageFromImage(cell.FriendsImageView)
        delegate.getRoundImageFromImage(cell.RounderImageView)
        
        cell.SeparateImageView.backgroundColor = kAppSecondaryColor
        cell.RounderImageView.backgroundColor = kAppSecondaryColor
        cell.SeparateImageView.backgroundColor = kAppSecondaryColor
        
        
        return cell
    }

    
    // MARK: - UITableViewDelegate methods
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if (tableView == friendsTableView) {
            
            let user:UserModel = delegate.loggedUser.friends!.objectAtIndex(indexPath.row) as! UserModel
            
            let newMissionVC:UINavigationController = delegate.missionStoryboard.instantiateViewControllerWithIdentifier("ModalMission") as! UINavigationController
            
            ((newMissionVC.viewControllers as NSArray).firstObject as! MissionViewController).selectedFriends.addObject(user)
            
            self.navigationController?.presentViewController(newMissionVC, animated: true, completion: nil)
            
        } else {
            
        }
    }
    
    // MARK: - SegmentedButtonDelegate methods
    
    func didSelectButtonAtIndex(index: Int) {
        if (index == 0){
            
            self.friendsTableView.hidden = false
            self.groupsTableView.hidden = true
        
        }
        
        else {
            self.friendsTableView.hidden = true
            self.groupsTableView.hidden = false
        
        
        }
    }

}
