//
//  FriendsTableViewCell.swift
//  IMHO
//
//  Created by MacBook Pro on 15/3/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import UIKit

class FriendsTableViewCell: UITableViewCell {

    @IBOutlet var RounderImageView: UIImageView!
    @IBOutlet var FriendsImageView: UIImageView!
    @IBOutlet var FriendsTitleLabel: UILabel!
    @IBOutlet weak var checkImageView: UIImageView!
    
    @IBOutlet var SeparateImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
