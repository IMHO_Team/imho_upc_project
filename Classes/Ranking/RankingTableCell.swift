//
//  RankingTableCell.swift
//  IMHO
//
//  Created by Xavier Comes Gomez on 11/3/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import UIKit

class RankingTableCell: UITableViewCell {

    
    @IBOutlet weak var rankUserName: UILabel!
    @IBOutlet weak var rankUserImage: UIImageView!
    @IBOutlet weak var rankUserImageBorder: UIImageView!
    @IBOutlet weak var rankUserPoints: UILabel!
    
}
