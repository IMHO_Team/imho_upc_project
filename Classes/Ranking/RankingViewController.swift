//
//  RankingViewController.swift
//  IMHO
//
//  Created by Xavier Comes Gomez on 11/3/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import UIKit

class RankingViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, SegmentedButtonDelegate {

    //Friends - World Tab
    @IBOutlet weak var segmentedButton: SegmentedButton!
    
    //First User
    @IBOutlet weak var rankingFirstUserName: UILabel!
    @IBOutlet weak var rankingFirstUserPoints: UILabel!
    @IBOutlet weak var rankingFirstImage: UIImageView!
    @IBOutlet weak var rankingFirstImageBorder: UIImageView!
    
    //Table View for Users list.
    @IBOutlet weak var rankingTableView: UITableView!

    //Imhometer Graphic
    @IBOutlet weak var rankImhometerView: UIView!
    @IBOutlet weak var imhometer4Label: UILabel!
    @IBOutlet weak var imhometer3Label: UILabel!
    @IBOutlet weak var imhometer2Label: UILabel!
    @IBOutlet weak var imhometer1Label: UILabel!
    
    @IBOutlet weak var rankingPointsBarView: UIView!
    var imhometerOrigin:CGFloat = 0.0
    var imhometerHeight: CGFloat = 0.0
    
    //Delegate
    var delegate:AppDelegate = (UIApplication.sharedApplication().delegate! as! AppDelegate)
    
    //User Lists
    var rankUsersList:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        
        // Do any additional setup after loading the view.

//Comentado para quitar temporalmente el ranking mundial. Solo dejaremos el ranking de amigos
//        segmentedButton.setupview(NSArray(objects: getLocalizedStringForKey("homeFriendsTabButton"), getLocalizedStringForKey("homeWorldTabButton")))
        
        
        segmentedButton.setupview(NSArray(object: getLocalizedStringForKey("rankFriendsTabButton")))
        segmentedButton.delegate = self
    
        // Setup colors, fonts, text, etc
        setupView()
        
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        //rankingPointsBarView.hidden = true

        //Call this method to reload the friends points.
        loadFriendsWithPoints()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - RankingViewController methods
    
    func setupView() {
        
        //First user formatting
        rankingFirstImageBorder.backgroundColor = kAppSecondaryColor
        delegate.getRoundImageFromImage(rankingFirstImage)
        delegate.getRoundImageFromImage(rankingFirstImageBorder)
        rankingFirstUserName.font = helveticaNeueLightWithSize(17)
        rankingFirstUserPoints.font = helveticaNeueLightWithSize(14)
        rankingFirstUserName.textColor = kAppTextColor
        rankingFirstUserPoints.textColor = kAppTextColor
        //Imhometer labels formatting
        imhometer1Label.font = helveticaNeueLightWithSize(15)
        imhometer1Label.textColor = kAppTextColor
        imhometer2Label.font = helveticaNeueLightWithSize(15)
        imhometer2Label.textColor = kAppTextColor
        imhometer3Label.font = helveticaNeueLightWithSize(15)
        imhometer3Label.textColor = kAppTextColor
        imhometer4Label.font = helveticaNeueLightWithSize(15)
        imhometer4Label.textColor = kAppTextColor
        imhometerOrigin = rankingPointsBarView.frame.origin.y
        imhometerHeight = rankingPointsBarView.frame.height
      
        
    }
    
    
    func drawImhometer(firstUserPoints:Int){
        /*  Height max 140 min 40 - 100pixels to play with height
            Points levels
            Newbie: 0 - 100 --> 20 pixels (from 1 to 20)
            Opinion Lover: 101 - 500 --> 20 pixels (from 21 to 40)
            Imho Pro: 501 - 1000 --> 20 pixels (from 41 to 60)
            God of Imho: 1001 - 2000 --> 20 pixels (from 61 to 80)
            2001 - Infinite
        */
        
        self.rankingPointsBarView.frame.size.height = imhometerHeight
        self.rankingPointsBarView.frame.origin.y = imhometerOrigin

        
        
        UIView.animateWithDuration(1.0, animations: {
            
            if(firstUserPoints<101){
                
                //Set height of view between 0 and 20 pixels
                self.rankingPointsBarView.frame.size.height += CGFloat(Int(firstUserPoints*20)/100)
                self.rankingPointsBarView.frame.origin.y -= CGFloat(Int(firstUserPoints*20)/100)
                
                self.imhometer1Label.textColor = UIColor.darkGrayColor()
                self.imhometer2Label.textColor = UIColor.lightGrayColor()
                self.imhometer3Label.textColor = UIColor.lightGrayColor()
                self.imhometer4Label.textColor = UIColor.lightGrayColor()
                
                
            }else if(firstUserPoints<501){
                
                //Set height of view between 21 and 40 pixels
                self.rankingPointsBarView.frame.size.height += CGFloat(Int(firstUserPoints*20/500))+57
                self.rankingPointsBarView.frame.origin.y -= CGFloat(Int(firstUserPoints*20/500))+57
                
                self.imhometer1Label.textColor = UIColor.lightGrayColor()
                self.imhometer2Label.textColor = UIColor.darkGrayColor()
                self.imhometer3Label.textColor = UIColor.lightGrayColor()
                self.imhometer4Label.textColor = UIColor.lightGrayColor()
                
            }else if(firstUserPoints<1001){
                
                //Set height of view between 41 and 60 pixels
                self.rankingPointsBarView.frame.size.height += CGFloat(Int(firstUserPoints*20/1000))+77
                self.rankingPointsBarView.frame.origin.y -= CGFloat(Int(firstUserPoints*20/1000))+77
                
                self.imhometer1Label.textColor = UIColor.lightGrayColor()
                self.imhometer2Label.textColor = UIColor.lightGrayColor()
                self.imhometer3Label.textColor = UIColor.darkGrayColor()
                self.imhometer4Label.textColor = UIColor.lightGrayColor()
                
            }else if(firstUserPoints<2001){
                
                //Set height of view between 61 and 80 pixels
                self.rankingPointsBarView.frame.size.height += CGFloat(Int(firstUserPoints*20/2000))+97
                self.rankingPointsBarView.frame.origin.y -= CGFloat(Int(firstUserPoints*20/2000))+97
                
                self.imhometer1Label.textColor = UIColor.lightGrayColor()
                self.imhometer2Label.textColor = UIColor.lightGrayColor()
                self.imhometer3Label.textColor = UIColor.lightGrayColor()
                self.imhometer4Label.textColor = UIColor.darkGrayColor()
                
            }else if(firstUserPoints>2000){
                
                //Set height of view between 81 and 100 pixels
                self.rankingPointsBarView.frame.size.height += 140
                self.rankingPointsBarView.frame.origin.y -= 140
                
                self.imhometer1Label.textColor = UIColor.lightGrayColor()
                self.imhometer2Label.textColor = UIColor.lightGrayColor()
                self.imhometer3Label.textColor = UIColor.lightGrayColor()
                self.imhometer4Label.textColor = UIColor.darkGrayColor()
                
            }
            
        
        })
        
    }
    
    
    
    // MARK: - UITableViewDataSource methods
    
    // Estos son los 3 metodos obligatorios para programar una tabla
    
    // Numero de secciones en la tabla (letras del abecedario)
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    // Numero de filas en cada seccion
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rankUsersList.count
    }
    // Vista de la celda
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var user:UserModel
        let cell:RankingTableCell = tableView.dequeueReusableCellWithIdentifier("rankUserCell") as! RankingTableCell
        
        
        
        if (segmentedButton.selectedIndex == 0){
            //Load friends ranking
           user = rankUsersList.objectAtIndex(indexPath.row) as! UserModel
            
            cell.rankUserImageBorder.backgroundColor = kAppSecondaryColor
            
            if (user.profileImageURL != nil) {
                print(user.profileImageURL)
                downloadImageWithURL(NSURL(string: user.profileImageURL!)!, andSuccessBlock: { (succeeded, image) -> () in
                    
                    cell.rankUserName.text = user.name
                    cell.rankUserPoints.text = String(user.rankPoints)
                    cell.rankUserImage.image = image
                    
                    self.delegate.getRoundImageFromImage(cell.rankUserImage)
                    self.delegate.getRoundImageFromImage(cell.rankUserImageBorder)
                    
                }, andErrorBlock: { (error) -> () in
                    cell.rankUserName.text = "User not found"
                    cell.rankUserPoints.text = "User not found"
                    cell.rankUserImage.image = nil
                })
            }
            
            
            
            
        }else{
           //Load world ranking
        //TODO :
           
        }
        
        
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 47
    }
    
    
    // MARK: - Load data method
    
    
    
    func loadFriendsWithPoints() {
        var userPoints:Int = 0
        
        //Set the real User friends List from Parse into the ContentArray (friends array, not te worlds one)
        delegate.loggedUser.getUserFriendsByPoints({ (friends) -> () in
            self.rankUsersList = friends
            
            let firstUser:UserModel = self.rankUsersList.firstObject as! UserModel
            userPoints = firstUser.rankPoints
            
            //Setting the FIRST User info and deletes from array.
            self.rankingFirstUserName.text = firstUser.name
            self.rankingFirstUserPoints.text = String(firstUser.rankPoints)
                downloadImageWithURL(NSURL(string: firstUser.profileImageURL!)!, andSuccessBlock: { (succeeded, image) -> () in
                    self.rankingFirstImage.image = image
                
                    }, andErrorBlock: { (error) -> () in
                })
            
            //After getting the first user info and image
            self.rankUsersList.removeObjectAtIndex(0)
            //Draw the imhometer
            self.drawImhometer(userPoints)
            
            self.rankingTableView.reloadData()
        })
        
        
        
        
    }

    
    // MARK: - SegmentedButtonDelegate methods
    
    func didSelectButtonAtIndex(index: Int) {
        
        if (index == 0){
            //Friends segment button selected
  
            
            //Reload the Table content
            //rankingTableView.reloadData()

            
            
        }else{
            //World segment button selected
            
            //Reload the Table content
            //rankingTableView.reloadData()
            
            
        }
        
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //CODE TO BE RUN ON CELL TOUCH
       
    //    var userSelected:UserModel?
        
        
      
        
        
//        if (segmentedButton.selectedIndex == 0){
//            //friends table
//            userSelected = contentArray.objectAtIndex(indexPath.item) as? UserModel
//            
//            
//        }else {
//            //world table
//            userSelected = contentWorldArray.objectAtIndex(indexPath.item) as? UserModel
//            
//            
//            
//        }
//        
//        if (userSelected != nil){
//            var profileStoryboard:UIStoryboard = UIStoryboard(name: "SB_Profile", bundle: nil)
//            var profileViewController:ProfileViewController = profileStoryboard.instantiateViewControllerWithIdentifier("profile") as! ProfileViewController
//            
//
//            self.navigationController!.pushViewController(profileViewController, animated: true)
//            profileViewController.setUser(userSelected!)
//            
//            
//        }
//        
        
    }
    
    
    
}
