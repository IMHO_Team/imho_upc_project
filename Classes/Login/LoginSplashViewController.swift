//
//  LoginSplashViewController.swift
//  IMHO
//
//  Created by Xavier Comes Gomez on 23/5/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import UIKit

class LoginSplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    /* Method that performs the facebook login, coming from the main login view controller to load the animation.
    
    
    */
    override func viewDidAppear(animated: Bool) {
        
        self.dismissViewControllerAnimated(false, completion: { () -> Void in
            
            (UIApplication.sharedApplication().delegate! as! AppDelegate).registerForPush()
            
            let homeStoryboard:UIStoryboard = UIStoryboard(name: "SB_Home", bundle: nil)
            
            var tabBarController:UITabBarController = homeStoryboard.instantiateViewControllerWithIdentifier("tabBar") as! UITabBarController
            
            (UIApplication.sharedApplication().delegate! as! AppDelegate).window!.rootViewController =
                (UIApplication.sharedApplication().delegate! as! AppDelegate).tabBarController! as UIViewController
        })

    }
    
}
