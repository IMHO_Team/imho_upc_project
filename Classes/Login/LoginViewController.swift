
//
//  LoginViewController.swift
//  IMHO
//
//  Created by Andrea Aranguren on 3/17/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import UIKit


class LoginViewController: UIViewController, CMSwitchViewDelegate {

    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var emailButton: UIButton!
    
    @IBOutlet weak var filterBlurImageView: UIImageView!
    
    //Nuevo login Switch
    @IBOutlet weak var facebookLoginTitle: UILabel!
    @IBOutlet weak var facebookLoginSwitch: CMSwitchView!

    @IBOutlet weak var googleLoginTitle: UILabel!
    @IBOutlet weak var googleLoginSwitch: CMSwitchView!
    
    //AnimationSegue
    var loginTransition = QZCircleSegue()
    @IBOutlet weak var buttonAnimation: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupView()
        
        //Declaration of the observer for the notification sender from CMSwitch class.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "loginSwitchesTitleDynamicAlpha:", name: kNotificationLoginSwitchesPan, object: nil)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: LoginViewController methods
    
    func setupView() {
        
        //MAIL Button - DEPRECATED
        self.emailButton.layer.cornerRadius = 20
        self.emailButton.layer.borderColor = UIColor.whiteColor().CGColor
        self.emailButton.layer.borderWidth = 1.0
        self.emailButton.backgroundColor = kAppMainColor.colorWithAlphaComponent(0.8)
        //Set up the blur background image
        //BlurImage
        self.filterBlurImageView.backgroundColor = kAppMainColor
        self.filterBlurImageView.alpha = 0.5
        //self.filterBlurImageView.hidden = false

        //Setting up the facebook login switch
        //Delegate
        facebookLoginSwitch.delegate = self
        //Facbook title Label init
        facebookLoginTitle.textColor = UIColor.whiteColor()
        facebookLoginTitle.alpha = 0
        //Facebook Switch init
        facebookLoginSwitch.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.4)
        facebookLoginSwitch.layer.cornerRadius = 20
        facebookLoginSwitch.borderColor = UIColor.clearColor()
        facebookLoginSwitch.dotColor = kAppLoginFBColor
        facebookLoginSwitch.color = UIColor.clearColor()
        facebookLoginSwitch.tintColor = kAppLoginFBColor
        facebookLoginSwitch.setFacebookIconInDot()
        facebookLoginSwitch.tag = 0
        
        /*The fingerprint for the ECDSA key sent by the server is 2f:03:e4:9b:49:52:31:aa:c2:bd:08:64:25:82:3c:a2.*/

        //Setting up Google+ login switch
        //Delegate
        googleLoginSwitch.delegate = self
        //Google+ title Label init
        googleLoginTitle.textColor = UIColor.whiteColor()
        googleLoginTitle.alpha = 0
        //Google+ Switch init
        googleLoginSwitch.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.4)
        googleLoginSwitch.layer.cornerRadius = 20
        googleLoginSwitch.borderColor = UIColor.clearColor()
        googleLoginSwitch.dotColor = kAppLoginGoogleColor
        googleLoginSwitch.color = UIColor.clearColor()
        googleLoginSwitch.tintColor = kAppLoginGoogleColor
        googleLoginSwitch.setGoogleIconInDot()
        googleLoginSwitch.tag = 1
        
        //Setting up the hidden button that permits the login animation transition.
        buttonAnimation.frame = CGRectMake(filterBlurImageView.frame.width/2, filterBlurImageView.frame.height/2, 40, 40)

    }
    

    
    /**
    Override the prepareForSegue to create the transition object when user performs the facebook login.
    param: the Segue and the sender.
    */
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let destinationViewController = segue.destinationViewController as! LoginSplashViewController
        self.loginTransition.animationDuration = 0.4
        self.loginTransition.animationColor = kAppMainColor
        self.loginTransition.animationChild = buttonAnimation
        self.loginTransition.fromViewController = self
        self.loginTransition.toViewController = destinationViewController
        destinationViewController.transitioningDelegate = loginTransition
    }
   
    
    @IBAction func facebookTapped() {
        

        
        DataSource.sharedInstance.facebookLoginWithSuccess({ (user) -> () in
            
            //Facebook login code moved to the segueController to perform the login succes after the segue!
            self.performSegueWithIdentifier("segueLogin", sender: nil)
            
        }, errorBlock: { (error) -> () in
            
        })
        
    }
    
    // MARK: CMSwitch Component methods
    
    
    // CMSwithDelegate
    func switchValueChanged(sender: AnyObject!, andNewValue value: Bool) {
        
        var timer:NSTimer = NSTimer()
        
        if (value == true){
            //When user clicks on the switch, animtaion starts with a 0.8seconds duration that dynamically sets the alpha label to 1
            UIView.animateWithDuration(0.8, animations: { () -> Void in
                //Check if the button pressed is Facebook button or google button
                if ((sender as! CMSwitchView) == self.facebookLoginSwitch){
                     self.facebookLoginTitle.alpha = 1
                    
                    timer = NSTimer.scheduledTimerWithTimeInterval(0.8, target: self, selector: Selector("facebookTapped"), userInfo: nil, repeats: false)

                }else if ((sender as! CMSwitchView) == self.googleLoginSwitch){
                    self.googleLoginTitle.alpha = 1
                }else{
                    print("Ninguno de los dos botones ha sido apretado.")
                }
            })
           
        }else{
            //Check if the button pressed is Facebook button or google button
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                //Check if the button pressed is Facebook button or google button
                if ((sender as! CMSwitchView) == self.facebookLoginSwitch){
                    self.facebookLoginTitle.alpha = 0
                }else if ((sender as! CMSwitchView) == self.googleLoginSwitch){
                    self.googleLoginTitle.alpha = 0
                }else{
                    print("Ninguno de los dos botones ha sido apretado.")
                }
            })
        }
    }

    
    
    
    // Method that receibes the notification from the Observer for login switches labels
    func loginSwitchesTitleDynamicAlpha(notification:NSNotification) {
        
        let userInfo = notification.userInfo!
        var timer:NSTimer = NSTimer()

        //If the button pressed is facbook button, the fb title alpha's dynaimally changes
        if (((userInfo as NSDictionary).objectForKey("viewTag") != nil) && ((userInfo as NSDictionary).objectForKey("viewTag")!.integerValue == 0)) {
        
            self.facebookLoginTitle.alpha = CGFloat((userInfo as NSDictionary).objectForKey("PanValue")!.floatValue) / 180
            
            if (((userInfo as NSDictionary).objectForKey("PanValue")!.floatValue) == 165.0){
                print("HE LLEGADO AL PUNTO MAXIMO Y ME LOGUEOOOOO!!!!!!")
                timer = NSTimer.scheduledTimerWithTimeInterval(0.8, target: self, selector: Selector("facebookTapped"), userInfo: nil, repeats: false)
                
            }
            
            
        //If the button pressed is google button, the fb title alpha's dynaimally changes
        }else if (((userInfo as NSDictionary).objectForKey("viewTag") != nil) && ((userInfo as NSDictionary).objectForKey("viewTag")!.integerValue == 1)) {
            
            self.googleLoginTitle.alpha = CGFloat((userInfo as NSDictionary).objectForKey("PanValue")!.floatValue) / 180
        
        }
        
        
    }
    
}
