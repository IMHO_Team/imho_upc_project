//
//  AppDelegate.swift
//  IMHO
//
//  Created by Rafael Zamora on 3/5/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import UIKit

import Parse
import Bolts
import Fabric
import Crashlytics


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var tabBarController:UITabBarController?
    var categoryList:NSMutableArray?
    
    var loggedUser:UserModel = UserModel()
    
    var homeStoryboard:UIStoryboard = UIStoryboard(name: "SB_Home", bundle: nil)
    var rankingStoryboard:UIStoryboard = UIStoryboard(name: "SB_Ranking", bundle: nil)
    var missionStoryboard:UIStoryboard = UIStoryboard(name: "SB_Mission", bundle: nil)
    var friendsStoryboard:UIStoryboard = UIStoryboard(name: "SB_Friends", bundle: nil)
    var profileStoryboard:UIStoryboard = UIStoryboard(name: "SB_Profile", bundle: nil)
    var loginStoryboard:UIStoryboard = UIStoryboard(name: "SB_Login", bundle: nil)
    
    var homeViewController:HomeViewController?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        //Crashlytics Integration
        Fabric.with([Crashlytics.self()])

        
        // Override point for customization after application launch.
        
        // Parse integration
        Parse.enableLocalDatastore()
        Parse.setApplicationId(kParseAppId,
            clientKey: kParseClientId)
        PFFacebookUtils.initializeFacebookWithApplicationLaunchOptions(launchOptions)
        // Track statistics around application opens.
        PFAnalytics.trackAppOpenedWithLaunchOptions(launchOptions)
        
        // AWS setup
        AWSServiceManager.defaultServiceManager().defaultServiceConfiguration = kAWSDefaultServiceConfiguration
        

        // Appearance
        setAppearance()
        
        tabBarController = homeStoryboard.instantiateViewControllerWithIdentifier("tabBar") as? UITabBarController
        
        let homeVC: AnyObject = (tabBarController!.viewControllers! as NSArray).objectAtIndex(0)
        homeViewController = ((homeVC as! UINavigationController).viewControllers as NSArray).objectAtIndex(0) as? HomeViewController
        let rankingVC:AnyObject = (rankingStoryboard.instantiateInitialViewController())!
        let missionVC:AnyObject = (missionStoryboard.instantiateInitialViewController())!
        let friendsVC:AnyObject = (friendsStoryboard.instantiateInitialViewController())!
        let profileVC:AnyObject = (profileStoryboard.instantiateInitialViewController())!
        
        let viewControllersArray:NSArray = NSArray(objects: homeVC, rankingVC, missionVC, friendsVC, profileVC)
        
        tabBarController!.viewControllers = viewControllersArray as? [UIViewController]
        
        
        //tabBarController!.selectedIndex = 2
        
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
    
        
//        //Eliminar esto para tener auto login
//        window!.rootViewController = loginStoryboard.instantiateInitialViewController() as? UIViewController
        
        //Piece o code for facebook login automativcally if u were logged before.
        if (PFUser.currentUser() == nil) {
            window!.rootViewController = loginStoryboard.instantiateInitialViewController() as UIViewController?//tabBarController
        } else {
            window!.rootViewController = tabBarController
            loggedUser = UserModel(parseUser: PFUser.currentUser()!)
            loggedUser.fetchFriends()
            loggedUser.getFriends({ (friends) -> () in
                
            })
        }
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        let currentInstalation:PFInstallation = PFInstallation.currentInstallation()
        currentInstalation.setDeviceTokenFromData(deviceToken)
        currentInstalation.channels = ["global",kChannelPrefix+self.loggedUser.identifier!]
        currentInstalation.saveInBackground()
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        
        // notificationid = 0 -> Mission time expired
        // notificationid = 1 -> All users answered
        // notificationid = 2 -> Someone sent you a mission
        // notificationid = 3 -> Someone answered your mission
        
        let notificationID:Int = (userInfo as NSDictionary).objectForKey("notificationid")!.integerValue
        
        print(notificationID)
        
        if notificationID == 2 {
            homeViewController!.loadFriendsData()
            self.tabBarController!.selectedIndex = 0
        } else {
            let missionID: String = (userInfo as NSDictionary).objectForKey("missionid") as! String
            homeViewController!.selectedMissionIdentifier = missionID
            homeViewController!.performSegueWithIdentifier("HomeViewToHomeMissionDetails", sender: nil)
            self.tabBarController!.selectedIndex = 0
        }
    }
    
    // MARK: Other methods
    
    func setAppearance() {
        
        UINavigationBar.appearance().barTintColor = kAppMainColor
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        UITabBar.appearance().tintColor = kAppMainColor
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        UINavigationBar.appearance().titleTextAttributes = titleDict as? [String : AnyObject]
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), forBarMetrics: UIBarMetrics.Default)
    }
    
    func getLocalizedStringForKey(key: String) -> String {
        
        return NSBundle.mainBundle().localizedStringForKey(key, value: "", table: nil)
    }
    
    func getRoundImageFromImage(imageView:UIImageView) {
        
        imageView.layer.cornerRadius = imageView.frame.height/2
        
    }
    
    func getCategoryListWithSuccessBlock(successBlock:(categories:NSMutableArray) -> ()) {
        
        if (categoryList == nil) {
            
            DataSource.sharedInstance.getCategoryListWithSuccessBlock({ (categories) -> () in
                
                self.categoryList = categories
                
                successBlock(categories: self.categoryList!)
            })
            
        } else {
            successBlock(categories: categoryList!)
        }
    }

    func registerForPush() {
        
        let notificationTypes:UIUserNotificationType = [UIUserNotificationType.Alert, UIUserNotificationType.Badge, UIUserNotificationType.Sound]
        
        let settings:UIUserNotificationSettings = UIUserNotificationSettings(forTypes: notificationTypes, categories: nil)
        
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        UIApplication.sharedApplication().registerForRemoteNotifications()
    }

}

