//
//  HomeCollectionCell.swift
//  IMHO
//
//  Created by Rafael Zamora on 3/22/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import UIKit

class HomeCollectionCell: UICollectionViewCell,BeanTransitionManagerCellExpanding {
    
    @IBOutlet weak var containerView: HomeCollectionContainerView!
    @IBOutlet weak var missionImageView: UIImageView!
    @IBOutlet weak var firstImageView: UIImageView!
    @IBOutlet weak var missionLabel: UILabel!
    @IBOutlet weak var secondImageView: UIImageView!
    @IBOutlet weak var separatorImageView: UIImageView!
    var cellImageView:UIImageView = UIImageView()
    
    func setImageView() {
        
        cellImageView = missionImageView
    }
    
}
