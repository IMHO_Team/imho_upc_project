//
//  HomeMissionCommentTableViewCell.swift
//  IMHO
//
//  Created by Erik Podetti on 06/09/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import Foundation

class HomeMissionCommentTableViewCell: UITableViewCell {
    
    @IBOutlet var AuthorImageView: UIImageView!
    @IBOutlet var CommentLabel: UILabel!
    @IBOutlet var CommentDateTimeLabel: UILabel!
    
    @IBOutlet var SeparateImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        
    }
    
}
