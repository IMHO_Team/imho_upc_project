//
//  HomeWorldViewController.swift
//  IMHO
//
//  Created by Rafael Zamora on 3/17/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import UIKit


class HomeWorldViewController: RPSlidingMenuViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - RPSlidingMenuViewController methods
    
    override func numberOfItemsInSlidingMenu() -> Int {
        return 10
    }
    
    override func customizeCell(slidingMenuCell: RPSlidingMenuCell!, forRow row: Int) {
        slidingMenuCell.textLabel.text = "Some text"
        slidingMenuCell.detailTextLabel.text = "Some larger text"
        slidingMenuCell.backgroundImageView.image = UIImage(named: "StackCarpetImage")
    }
    
    override func slidingMenu(slidingMenu: RPSlidingMenuViewController!, didSelectItemAtRow row: Int) {
    
    }

}
