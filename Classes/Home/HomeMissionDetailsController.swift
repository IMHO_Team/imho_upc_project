//
//  HomeMissionDetailsController.swift
//  IMHO
//
//  Created by Andrea Aranguren on 6/3/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import UIKit


class HomeMissionDetailsController: UIViewController {
    
    let kDefaultFilterAlpha:CGFloat = 0.5
    var selectedMission:MissionModel?
    var delegate:AppDelegate = (UIApplication.sharedApplication().delegate! as! AppDelegate)
    @IBOutlet weak var commentOrFinishButton: UIButton!
    @IBOutlet weak var unansweredCountTextView: UILabel!
    @IBOutlet weak var timeLeftTextView: UILabel!
    @IBOutlet weak var dislikeCountTextView: UILabel!
    @IBOutlet weak var likeCountTextView: UILabel!
    @IBOutlet weak var questionTextView: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var firstImageView: UIImageView!
    @IBOutlet weak var secondImageView: UIImageView!
    @IBOutlet weak var separatorImageView: UIImageView!
    @IBOutlet weak var filterImageView: UIImageView!
    @IBOutlet weak var likesImageView: UIImageView!
    @IBOutlet weak var dislikesImageView: UIImageView!
    

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var isMissionLoaded:Bool = false
    var missionIdentifier:String?
    var commentsToShow:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commentsToShow = NSMutableArray()
        if isMissionLoaded {
            
            setupLoadedMission()
        } else {
            
            self.fetchMission()
        }
        
        // Hide loading at the beginning
        self.filterImageView.alpha = 0
        self.activityIndicator.alpha = 0
        self.filterImageView.hidden = true
        self.activityIndicator.hidden = true
        
        self.startLoading()

    }
    
    func startLoading() {
        
        self.activityIndicator.startAnimating()
        
        self.filterImageView.hidden = false
        self.activityIndicator.hidden = false
        
        UIView.animateWithDuration(1.0, animations: { () -> Void in
            self.filterImageView.alpha = self.kDefaultFilterAlpha
            self.activityIndicator.alpha = 1
        })
    }
    
    func stopLoading() {
        
        self.activityIndicator.stopAnimating()
        
        UIView.animateWithDuration(1.0, animations: { () -> Void in
            self.filterImageView.alpha = 0
            self.activityIndicator.alpha = 0
            
            self.filterImageView.hidden = true
            self.activityIndicator.hidden = true
        })
        
    }

    
    func setupLoadedMission() {
        
        self.title = selectedMission?.status
        if (selectedMission!.images!.count == 1) {
            let imageModel:ImageModel = (selectedMission!.images!.objectAtIndex(0) as? ImageModel)!
            imageView.layer.masksToBounds = true
            imageView.image = imageModel.image
            separatorImageView.hidden = true
            
        } else {
            let firstImageModel:ImageModel = (selectedMission!.images!.objectAtIndex(0) as? ImageModel)!
            firstImageView.layer.masksToBounds = true
            firstImageView.image = firstImageModel.image
            
            let secondImageModel:ImageModel = (selectedMission!.images!.objectAtIndex(1) as? ImageModel)!
            secondImageView.layer.masksToBounds = true
            secondImageView.image = secondImageModel.image
        }
        questionTextView.text = selectedMission?.question
        if(selectedMission?.user?.identifier == delegate.loggedUser.identifier){
            commentOrFinishButton.setTitle(getLocalizedStringForKey("endMission"), forState: UIControlState.Normal)
            print(selectedMission?.status, terminator: "");
            if(selectedMission?.status == kParseValueMissionStatusClosed){
                commentOrFinishButton.setTitle(getLocalizedStringForKey("commentsInMission"), forState: UIControlState.Normal)
            }
        }else{
            commentOrFinishButton.hidden = true
        }
        
        
        getMissionAnswers()
        
        _ = selectedMission?.length!
        let missionCreation:NSDate = selectedMission!.createdAt!
        let currentDate:NSDate = NSDate()
        let userCalendar = NSCalendar.currentCalendar()
        let expirationDate = userCalendar.dateByAddingUnit(NSCalendarUnit.Day, value: selectedMission!.length!, toDate: missionCreation,  options: [])!
        
        
        
        if(expirationDate.isGreaterThanDate(currentDate)){
            let remaining = currentDate.offsetTo(expirationDate)
            timeLeftTextView.text = remaining
        }
        
        if (selectedMission!.images!.count > 1) {
            
            likesImageView.image = UIImage(named: "VersusDetailsRight")
            dislikesImageView.image = UIImage(named: "VersusDetailsLeft")
            
        }
    }
    
    
    func getMissionAnswers(){
        selectedMission?.getMissionAnswersWithSuccessBlock({ (array) -> () in
            var likes = 0
            var dislikes=0
            
            
            for object in array{
                let answer:AnswerModel = object as! AnswerModel
                if(answer.comment != nil && !(answer.comment == "")){
                    self.commentsToShow.addObject(answer)
                }
                
                if((answer.singleAnswer) != nil){
                    if(answer.singleAnswer?.boolValue == true){
                        likes++
                    }else{
                        dislikes++
                    }
                    
                }
            }
            
            if(self.selectedMission?.user?.identifier == self.delegate.loggedUser.identifier && self.commentsToShow.count < 1 && self.selectedMission?.status == kParseValueMissionStatusClosed ){
                self.commentOrFinishButton.hidden = true
                
            }
            
            self.stopLoading()
            
            let missing:Int = (self.selectedMission!.singleUsers!.count - array.count) as Int
            
            self.unansweredCountTextView.text = String(missing)
            self.likeCountTextView.text = String(likes)
            self.dislikeCountTextView.text = String(dislikes)
            
            
        })

    }
    
    func fetchMission() {
        
        DataSource.sharedInstance.fetchMission(missionIdentifier!, withSuccess: { (mission) -> () in
            
            self.selectedMission = mission
            self.setupLoadedMission()
            
            if (mission.images!.count == 1) {
                
                self.separatorImageView.hidden = true
                let image:ImageModel = mission.images!.objectAtIndex(0) as! ImageModel
                
                image.getImageWithBlock({ (image) -> () in
                    
                    self.imageView.image = image;
                    
                })
            } else {
                
                let firstImage:ImageModel = mission.images!.objectAtIndex(0) as! ImageModel
                
                firstImage.getImageWithBlock({ (image) -> () in
                    self.firstImageView.image = image;
                })
                
                let secondImage:ImageModel = mission.images!.objectAtIndex(1) as! ImageModel
                
                secondImage.getImageWithBlock({ (image) -> () in
                    self.secondImageView.image = image;
                })
            }
            
            }, errorBlock: { (error) -> () in
                
        })
    }
    
    @IBAction func onCommentOrFinishedClick(sender: AnyObject) {
        if(selectedMission?.status == kParseValueMissionStatusClosed){
            //TODO see comments
            self.performSegueWithIdentifier("missionComments", sender: nil)
           
        }else{
            self.selectedMission!.closeMissionWithSuccess({ () -> () in
              
                self.commentOrFinishButton.setTitle(getLocalizedStringForKey("commentsInMission"), forState: UIControlState.Normal)
                //self.getMissionAnswers()
                }, errorBlock: { (error) -> () in
                    print("Error closing mission", terminator: "")
            })
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if(segue.identifier == "missionComments" && self.commentsToShow.count>0) {
            
            let navController = segue.destinationViewController as! UINavigationController
            let commentsViewController = navController.topViewController as! HomeMissionCommentsController
            commentsViewController.commentsToShow = self.commentsToShow
        }
    }
    
    
}
