//
//  HomeViewController.swift
//  IMHO
//
//  Created by Rafael Zamora on 3/8/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, SegmentedButtonDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIActionSheetDelegate {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet weak var segmentedButtonView: SegmentedButton!
    @IBOutlet weak var homeCollectionView: UICollectionView!
    
    var friendsMissionArray:NSMutableArray = NSMutableArray()
    var youMissionArray:NSMutableArray = NSMutableArray()
    var worldArray:NSMutableArray = NSMutableArray()
    var beanTransitionManager:BeanTransitionManager = BeanTransitionManager(transitionDuration: 0.5)
    
    var delegate:AppDelegate = (UIApplication.sharedApplication().delegate! as! AppDelegate)
    
    var selectedMission:MissionModel?
    var selectedMissionIdentifier:String?
    
    var draggableBackground:DraggableViewBackground?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loadFriendsData()
        loadYouData()
        
        delegate.getCategoryListWithSuccessBlock { (categories) -> () in
            self.worldArray = categories.mutableCopy() as! NSMutableArray
            self.homeCollectionView.reloadItemsAtIndexPaths(NSArray(object: NSIndexPath(forItem: 1, inSection: 0)) as! [NSIndexPath])
        }
        
        self.segmentedButtonView.setupview(NSArray(objects: getLocalizedStringForKey("homeFriendsTabButton"), getLocalizedStringForKey("homeWorldTabButton"), getLocalizedStringForKey("homeYouTabButton")))
        self.segmentedButtonView.delegate = self
        
        registerNotifications()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "forwardTapped:", name: kNotificationForwardTapped, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "shareTapped:", name: kNotificationShareTapped, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "moreTapped:", name: kNotificationMoreTapped, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "commentTapped:", name: kNotificationDidSelectComment, object: nil)
        
        //Reload missions in You Tab.
        if (segmentedButtonView.selectedIndex == 2){
            //call loadYouData to reload the YOU missions array.
            loadYouData()
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: kNotificationForwardTapped, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: kNotificationShareTapped, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: kNotificationMoreTapped, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: kNotificationDidSelectComment, object: nil)
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if (segue.identifier! == "homeWorldDetail") {
            
            (segue.destinationViewController as! HomeWorldDetailViewController).category = (sender as! NSDictionary).objectForKey("category") as? CategoryModel
        } else if (segue.identifier! == "homeForward") {
            
            (((segue.destinationViewController as! UINavigationController).viewControllers as NSArray).firstObject as! HomeForwardViewController).mission = sender as? MissionModel
        } else if (segue.identifier! == "homeDetail") {
            
            
        } else if (segue.identifier! == "HomeViewToHomeMissionDetails"){
            
            if self.selectedMission != nil {
                (segue.destinationViewController as! HomeMissionDetailsController).isMissionLoaded = true
                (segue.destinationViewController as! HomeMissionDetailsController).selectedMission = self.selectedMission
            } else {
                (segue.destinationViewController as! HomeMissionDetailsController).missionIdentifier = self.selectedMissionIdentifier
            }
        } else if (segue.identifier! == "homeComment") {
            (((segue.destinationViewController as! UINavigationController).viewControllers as NSArray).objectAtIndex(0) as! HomeCommentViewController).mission = (sender as! MissionModel)
        }
    }


    // MARK: - HomeViewController methods
    
    func registerNotifications() {
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "answerDidPost:", name: kNotificationAnswerDidPost, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "didSelectCategory:", name: kNotificationDidSelectCategory, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "didSelectMission:", name: kNotificationDidSelectMission, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "didAddComment:", name: kNotificationDidAddComment, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "createNewMission:", name: kNotificationCreateMission, object: nil)
    }
    
    func loadFriendsData() {
        
        delegate.loggedUser.getNewMissions { (missions) -> () in
            self.friendsMissionArray = missions.mutableCopy() as! NSMutableArray
            self.homeCollectionView.reloadData()
        }
    }
    
    func loadYouData() {
        
        self.youMissionArray = NSMutableArray()

        delegate.loggedUser.getCurrentMissions { (missions) -> () in
            self.youMissionArray.addObjectsFromArray(missions.mutableCopy() as! [AnyObject])
            self.homeCollectionView.reloadItemsAtIndexPaths(NSArray(object: NSIndexPath(forItem: 2, inSection: 0)) as! [NSIndexPath])
        }
        
        delegate.loggedUser.getOwnCurrentMissions { (missions) -> () in
            self.youMissionArray.addObjectsFromArray(missions.mutableCopy() as! [AnyObject])
            self.homeCollectionView.reloadItemsAtIndexPaths(NSArray(object: NSIndexPath(forItem: 2, inSection: 0)) as! [NSIndexPath])
        }
    }
    
    func answerDidPost(notification:NSNotification) {
        
        //var missionAnswered:MissionModel = (notification.us
        
        let dissapearedMission:MissionModel = (notification.userInfo! as NSDictionary).objectForKey("mission") as! MissionModel

        DataSource.sharedInstance.notifyNewAnswerToUser(dissapearedMission.user!, withAnswer: (notification.userInfo! as NSDictionary).objectForKey("answer")!.boolValue, andMission: dissapearedMission)
        
        for object in friendsMissionArray {
            
            let mission:MissionModel = object as! MissionModel
            
            if (mission.objectId! == dissapearedMission.objectId! ) {
                friendsMissionArray.removeObject(mission)
                break
            }
        }
        
        loadYouData()
    }
    
    func didSelectCategory(notification:NSNotification) {
        
        self.performSegueWithIdentifier("homeWorldDetail", sender: notification.userInfo!)
    }
    
    func forwardTapped(notification:NSNotification) {
        
        self.performSegueWithIdentifier("homeForward", sender: (notification.userInfo! as NSDictionary).objectForKey("mission"))
    }
    
    func shareTapped(notification:NSNotification) {
     
        let mission:MissionModel = (notification.userInfo! as NSDictionary).objectForKey("mission") as! MissionModel

        presentShareOnViewController(self, forMission: mission)
    }
    
    func moreTapped(notification:NSNotification) {
        
        let actionSheet:UIActionSheet = UIActionSheet()
        
        actionSheet.addButtonWithTitle(getLocalizedStringForKey("moreActionSheetSaveForLater"))
        actionSheet.addButtonWithTitle(getLocalizedStringForKey("moreActionSheetCancel"))
        actionSheet.delegate = self
        actionSheet.cancelButtonIndex = 1
        actionSheet.tag = 0
        actionSheet.showInView(self.view)
        
    }
    
    func commentTapped(notification:NSNotification) {
        
        self.performSegueWithIdentifier("homeComment", sender: (notification.userInfo! as NSDictionary).objectForKey("mission"))
    }
    
    func didAddComment(notification:NSNotification) {
        
        self.draggableBackground!.addCommentToLastCard((notification.userInfo! as NSDictionary).objectForKey("comment") as! String)
    }
    
    func didSelectMission(notification:NSNotification) {
        
        
        self.selectedMission = (notification.userInfo! as NSDictionary).objectForKey("mission") as? MissionModel
        self.performSegueWithIdentifier("HomeViewToHomeMissionDetails", sender: nil)
//        if (self.selectedMission!.user!.identifier == delegate.loggedUser.identifier!) {
//            var actionSheet:UIActionSheet = UIActionSheet()
//            
//            actionSheet.addButtonWithTitle(getLocalizedStringForKey("extraActionSheetCloseMission"))
//            actionSheet.addButtonWithTitle(getLocalizedStringForKey("moreActionSheetCancel"))
//            actionSheet.delegate = self
//            actionSheet.cancelButtonIndex = 1
//            actionSheet.tag = 1
//            actionSheet.showInView(self.view)
//        }
        
        
    }
    
    func createNewMission(notification:NSNotification) {
        
        self.delegate.tabBarController!.selectedIndex = 2
    }
    
    // MARK: - HomeViewController actions
    
    func didSelectButtonAtIndex(index: Int) {
        
        if (index == 0) {
            self.homeCollectionView.scrollToItemAtIndexPath(NSIndexPath(forItem: index, inSection: 0), atScrollPosition: UICollectionViewScrollPosition.Left, animated: true)
        } else if (index == 1) {
            
            self.homeCollectionView.scrollToItemAtIndexPath(NSIndexPath(forItem: index, inSection: 0), atScrollPosition: UICollectionViewScrollPosition.Left, animated: true)
            
        } else {
            self.homeCollectionView.scrollToItemAtIndexPath(NSIndexPath(forItem: index, inSection: 0), atScrollPosition: UICollectionViewScrollPosition.Left, animated: true)
        }
    }
    
    // MARK: - UICollectionViewDataSource methods
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        var cell:HomeCollectionCell?
        
        
        if (indexPath.item == 0) {
            // Add DraggableViewBackground for cards like behavior
            
            cell = collectionView.dequeueReusableCellWithReuseIdentifier("HomeCollectionCell", forIndexPath: indexPath) as? HomeCollectionCell
            
            var frame:CGRect = cell!.frame
            
            frame.size.height = frame.size.height*1
            
            self.draggableBackground = DraggableViewBackground(frame: frame, andArray: friendsMissionArray)
            
            cell!.insertSubview(draggableBackground!, belowSubview: self.segmentedButtonView)
        }
        else if (indexPath.item == 1) {
            cell = collectionView.dequeueReusableCellWithReuseIdentifier("HomeCollectionWorldCell", forIndexPath: indexPath) as? HomeCollectionCell
            
            cell!.containerView.setupWorld(worldArray)
        }
        else {
            cell = collectionView.dequeueReusableCellWithReuseIdentifier("HomeCollectionYouCell", forIndexPath: indexPath) as? HomeCollectionCell
            
            cell!.containerView.setupYou(youMissionArray)
            
        }
        
        return cell!
    }
    
    // MARK: - UICollectionViewDelegate methods
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return self.homeCollectionView.frame.size
    }
    
    // MARK: - UIScrollViewDelegate methods
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        
        let pageWidth:CGFloat = self.homeCollectionView.frame.size.width
        let selectedIndex = self.homeCollectionView.contentOffset.x/pageWidth
        self.segmentedButtonView.setSelectedTab(Int(selectedIndex))
    }
    
    // MARK: - UIActionSheetDelegate methods
    
    func actionSheet(actionSheet: UIActionSheet, didDismissWithButtonIndex buttonIndex: Int) {
        
        if (actionSheet.tag == 0) {
            if (buttonIndex == 0) {
                
                self.friendsMissionArray.removeObjectAtIndex(0)
                
                NSNotificationCenter.defaultCenter().postNotificationName(kNotificationSaveMissionForLater, object: nil)
            }
        } else if (actionSheet.tag == 1) {
            	
            if (buttonIndex == 0) {
                self.selectedMission!.closeMissionWithSuccess({ () -> () in
                    
                    self.youMissionArray.removeObject(self.selectedMission!)
                    self.homeCollectionView.reloadData()
                    
                }, errorBlock: { (error) -> () in
                    
                })
            }
            
        }
    }

}
