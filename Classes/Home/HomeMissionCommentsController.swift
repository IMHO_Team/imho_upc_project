//
//  HomeMissionCommentsController.swift
//  IMHO
//
//  Created by Erik Podetti on 05/09/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import Foundation

class HomeMissionCommentsController: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    
    @IBOutlet weak var doneButton: UIBarButtonItem!
    @IBOutlet weak var commentsTableView: UITableView!
    
    var delegate:AppDelegate = (UIApplication.sharedApplication().delegate! as! AppDelegate)
    //var friends:NSMutableArray = NSMutableArray()
    //var indexArray:NSMutableArray = NSMutableArray()
    //var friendsToShow:NSMutableArray = NSMutableArray()
    var mission:MissionModel?                               // the selected mission we want to show comments for
    var commentsToShow:NSMutableArray = NSMutableArray()    // this will contain comments object for the selected mission
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setupView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - HomeForwardViewController methods
    
    func setupView()
    {
        // Set screen title
        self.title = getLocalizedStringForKey("commentsInMission")
    }
    
    // MARK: - UITableViewDataSource methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.commentsToShow.count
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell:HomeMissionCommentTableViewCell = tableView.dequeueReusableCellWithIdentifier("MissionCommentCell") as! HomeMissionCommentTableViewCell
        
        
        let answer = (self.commentsToShow.objectAtIndex(indexPath.item)) as? AnswerModel
        cell.CommentLabel.text = answer?.comment
        answer?.user!.getProfileImageWithSuccess({ (image) -> () in
            
            cell.AuthorImageView.image = image
            self.delegate.getRoundImageFromImage(cell.AuthorImageView)
            cell.AuthorImageView.clipsToBounds = true
            
            }, errorBlock: { (error) -> () in
                
        })
        let spentTime = (answer!.createdAt)!.offsetTo(NSDate())
        cell.CommentDateTimeLabel.text = getLocalizedStringForKey("published") + " \(spentTime) " + getLocalizedStringForKey("ago")
        //
        return cell
    }
    
    // MARK: - UITableViewDelegate methods
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        
    }
    
    // MARK: - HomeMissionCommentsViewController actions
    
    @IBAction func backTapped(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
}
