//
//  HomeCollectionContainerView.swift
//  IMHO
//
//  Created by Rafael Zamora on 3/22/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import UIKit

class HomeCollectionContainerView: UIView, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var dataCollectionView:UICollectionView!
    @IBOutlet var dataTableView:UITableView!
    
    var missionArray:NSMutableArray = NSMutableArray()
    var categoryArray:NSMutableArray = NSMutableArray()
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    func setupYou(aData:NSMutableArray) {
        
        missionArray = aData
        self.dataCollectionView.reloadData()
    }
    
    
    func setupWorld(aData:NSMutableArray) {
        categoryArray = aData
        //dataTableView.reloadData()
    }
    
    
    // MARK: - UICollectionViewDataSource methods
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return missionArray.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell:HomeCollectionCell = collectionView.dequeueReusableCellWithReuseIdentifier("HomeDataCell", forIndexPath: indexPath) as! HomeCollectionCell
        
        let mission:MissionModel =  missionArray.objectAtIndex(indexPath.item) as! MissionModel
        
        cell.firstImageView.image = nil
        cell.secondImageView.image = nil
        cell.missionImageView.image = nil
        
        if (mission.images!.count == 1) {
            let image:ImageModel = mission.images!.objectAtIndex(0) as! ImageModel
            
            image.getImageWithBlock({ (image) -> () in
                UIView.animateWithDuration(0.8, animations: { () -> Void in
                    cell.missionImageView.image = image
                })
            })
            
            cell.firstImageView.hidden = true
            cell.secondImageView.hidden = true
            cell.missionImageView.hidden = false
            cell.separatorImageView.hidden = true
            
        } else {
            
            let firstImage:ImageModel = mission.images!.objectAtIndex(0) as! ImageModel
            
            firstImage.getImageWithBlock({ (image) -> () in
                UIView.animateWithDuration(0.8, animations: { () -> Void in
                    cell.firstImageView.image = image
                })
            })
            
            let secondImage:ImageModel = mission.images!.objectAtIndex(1) as! ImageModel
            
            secondImage.getImageWithBlock({ (image) -> () in
                UIView.animateWithDuration(0.8, animations: { () -> Void in
                    cell.secondImageView.image = image
                })
            })
            
            cell.firstImageView.hidden = false
            cell.secondImageView.hidden = false
            cell.missionImageView.hidden = true
            cell.separatorImageView.hidden = false
            
            //Formatting images to respect the aspect ratio.
            cell.firstImageView.layer.masksToBounds = true
            cell.secondImageView.layer.masksToBounds = true
            cell.missionImageView.layer.masksToBounds = true
            cell.firstImageView.contentMode = .ScaleAspectFill
            cell.secondImageView.contentMode = .ScaleAspectFill
            cell.missionImageView.contentMode = .ScaleAspectFill

            
        }
        
        if (mission.isCompleted == true) {
            cell.missionLabel.text = "Done"
        }
        else {
            let spentTime = (mission.createdAt)!.offsetTo(NSDate())
            cell.missionLabel.text = "\(spentTime) ago"
        }
        
        cell.missionLabel.font = helveticaNeueLightWithSize(15)
        
        cell.setImageView()
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout methods
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        let cellWidth:CGFloat = self.frame.size.width/2
        
        return CGSizeMake(cellWidth-1, cellWidth)
    }
    
    // MARK: - UICollectionViewDelegate
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let mission:MissionModel =  missionArray.objectAtIndex(indexPath.item) as! MissionModel
        
      NSNotificationCenter.defaultCenter().postNotificationName(kNotificationDidSelectMission, object: nil, userInfo: NSDictionary(object: mission, forKey: "mission") as [NSObject : AnyObject])
    }
    
    // MARK: - UITableViewDataSource methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    // Numero de filas en cada seccion
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.categoryArray.count
    }
    // Vista de la celda
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell:HomeTableCell = tableView.dequeueReusableCellWithIdentifier("HomeTableViewCell") as! HomeTableCell
        
        let category:CategoryModel = self.categoryArray.objectAtIndex(indexPath.row) as! CategoryModel
        cell.titleLabel.text = category.name!
        
        category.image!.getImageWithBlock { (image) -> () in
            cell.backgroundImageView.image = image
        }
        
        cell.titleLabel.font = helveticaNeueLightWithSize(26)
        
        
        return cell
    }
    // Genis Cormand
    // MARK: - UITableViewDelegate methods
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let category: AnyObject = self.categoryArray.objectAtIndex(indexPath.row)
        
        NSNotificationCenter.defaultCenter().postNotificationName(kNotificationDidSelectCategory, object: nil, userInfo: NSDictionary(object: category, forKey: "category") as [NSObject : AnyObject])
    }
    

}
