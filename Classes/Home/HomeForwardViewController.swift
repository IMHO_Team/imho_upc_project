//
//  HomeForwardViewController.swift
//  IMHO
//
//  Created by Rafael Zamora on 5/19/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import UIKit

class HomeForwardViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {

    @IBOutlet weak var sendButton: UIBarButtonItem!
    @IBOutlet weak var friendsTableView: UITableView!
    
    var delegate:AppDelegate = (UIApplication.sharedApplication().delegate! as! AppDelegate)
    var friends:NSMutableArray = NSMutableArray()
    var indexArray:NSMutableArray = NSMutableArray()
    var friendsToShow:NSMutableArray = NSMutableArray()
    var mission:MissionModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setupView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - HomeForwardViewController methods
    
    func setupView() {
        
        // Set screen title
        self.title = getLocalizedStringForKey("addFriendsTitle")
        
        self.sendButton.enabled = false
        
        for user in delegate.loggedUser.friends! {
            
            var added:Bool = false
            
            for userAdded in self.mission!.singleUsers! {
                
                if ((user as! UserModel).identifier! == (userAdded as! UserModel).identifier!) {
                    added = true
                }
            }
            
            if (added == false) {
                self.friendsToShow.addObject(user)
            }
        }
    }
    
    // MARK: - UITableViewDataSource methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.friendsToShow.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:FriendsTableViewCell = tableView.dequeueReusableCellWithIdentifier("ForwardFriendCell") as! FriendsTableViewCell
        
        let friend:UserModel = self.friendsToShow.objectAtIndex(indexPath.row) as! UserModel
        
        cell.FriendsTitleLabel.text = friend.name!
        
        friend.getProfileImageWithSuccess({ (image) -> () in
            
            cell.FriendsImageView.image = image
            
            }, errorBlock: { (error) -> () in
                
        })
        
        if (indexArray.containsObject(indexPath.row)) {
            cell.checkImageView.hidden = false
        } else {
            cell.checkImageView.hidden = true
        }
        
        delegate.getRoundImageFromImage(cell.FriendsImageView)
        cell.SeparateImageView.backgroundColor = kAppSecondaryColor
        
        
        return cell
    }
    
    // MARK: - UITableViewDelegate methods
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if (self.indexArray.containsObject(indexPath.item)) {
            self.indexArray.removeObject(indexPath.item)
            self.friends.removeObject(friendsToShow.objectAtIndex(indexPath.row))
        } else {
            self.indexArray.addObject(indexPath.item)
            self.friends.addObject(friendsToShow.objectAtIndex(indexPath.row))
        }
        
        if (self.indexArray.count > 0) {
            self.sendButton.enabled = true
        } else {
            self.sendButton.enabled = false
        }
        
        self.friendsTableView.reloadData()
    }
    
    // MARK: - HomeForwardViewController actions
    
    @IBAction func cancelTapped(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func sendTapped(sender: AnyObject) {
        
        self.mission!.forwardToUsers(self.friends, withSuccess: { () -> () in
            
            NSNotificationCenter.defaultCenter().postNotificationName(kNotificationDidForwardMission, object: nil, userInfo: NSDictionary(object: self.mission!, forKey: "mission") as [NSObject : AnyObject])
            
            self.dismissViewControllerAnimated(true, completion: nil)
            
        }) { (error) -> () in
            
        }
    }

}
