//
//  HomeCommentViewController.swift
//  IMHO
//
//  Created by Rafael Zamora on 9/19/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import UIKit

class HomeCommentViewController: UIViewController,UITextFieldDelegate {

    var mission:MissionModel?
    @IBOutlet weak var doneButton: UIBarButtonItem!
    @IBOutlet weak var commentTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.doneButton.enabled = false
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "textFieldDidChange:", name: UITextFieldTextDidChangeNotification, object: self.commentTextField)
        
        self.title = getLocalizedStringForKey("newCommentTitle")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.commentTextField.becomeFirstResponder()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func doneTapped(sender: AnyObject) {
        
        self.view.endEditing(true)
        NSNotificationCenter.defaultCenter().postNotificationName(kNotificationDidAddComment, object: nil, userInfo: NSDictionary(object: self.commentTextField.text!, forKey: "comment") as [NSObject : AnyObject])
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    @IBAction func cancelTapped(sender: AnyObject) {
        
        self.view.endEditing(true)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - UITextViewDelegate methods
    
    func textFieldDidChange(notification:NSNotification) {
        if (self.commentTextField.text != "") {
            self.doneButton.enabled = true
        } else {
            self.doneButton.enabled = false
        }
    }
}
