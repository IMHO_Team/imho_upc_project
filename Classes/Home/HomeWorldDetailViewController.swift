//
//  HomeWorldDetailViewController.swift
//  IMHO
//
//  Created by Rafael Zamora on 4/22/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import UIKit

class HomeWorldDetailViewController: UIViewController {

    var category:CategoryModel?
    var missionsArray:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupView()
        
        loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "forwardTapped:", name: kNotificationForwardTapped, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "shareTapped:", name: kNotificationShareTapped, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: kNotificationForwardTapped, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: kNotificationShareTapped, object: nil)
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if (segue.identifier == "detailForward") {
            (((segue.destinationViewController as! UINavigationController).viewControllers as NSArray).firstObject as! HomeForwardViewController).mission = sender as? MissionModel
        }
    }


    // HomeWorldDetailViewController methods
    
    func forwardTapped(notification:NSNotification) {
        
        self.performSegueWithIdentifier("detailForward", sender: (notification.userInfo! as NSDictionary).objectForKey("mission"))
    }
    
    func shareTapped(notification:NSNotification) {
        
        let mission:MissionModel = (notification.userInfo! as NSDictionary).objectForKey("mission") as! MissionModel
        
        presentShareOnViewController(self, forMission: mission)
    }
    
    func setupView() {
    
        self.title = self.category!.name!
    }
    
    func loadData() {
        
        category!.getPublicMissionsWithSuccessBlock({ (array) -> () in
            
            self.missionsArray = array.mutableCopy() as! NSMutableArray
            
            var frame:CGRect = self.view.frame
            frame.size.height = frame.size.height - 80
            frame.origin.y = frame.origin.y + 40
            
            let draggableBackground:DraggableViewBackground = DraggableViewBackground(frame: frame, andArray: self.missionsArray)
            self.view.addSubview(draggableBackground)
        })
    }
}
