//
//  ProfileCollectionCell.swift
//  IMHO
//
//  Created by Rafael Zamora on 5/20/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import UIKit

class ProfileCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var missionsTitleLabel: UILabel!
    @IBOutlet weak var missionsValueLabel: UILabel!
    @IBOutlet weak var friendsTitleLabel: UILabel!
    @IBOutlet weak var friendsValueLabel: UILabel!
    @IBOutlet weak var pointsTitleLabel: UILabel!
    @IBOutlet weak var pointsValueLabel: UILabel!
    @IBOutlet weak var borderImageView: UIImageView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    func setup() {
        
        missionsTitleLabel.text = getLocalizedStringForKey("missionsLabelTitle")
        friendsTitleLabel.text = getLocalizedStringForKey("friendsLabelTitle")
        pointsTitleLabel.text = getLocalizedStringForKey("pointsLabelTitle")
        
        applyFonts()
    }
    
    func applyFonts() {
        
        missionsTitleLabel.font = helveticaNeueLightWithSize(15)
        friendsTitleLabel.font = helveticaNeueLightWithSize(15)
        pointsTitleLabel.font = helveticaNeueLightWithSize(15)
        missionsValueLabel.font = helveticaNeueLightWithSize(15)
        friendsValueLabel.font = helveticaNeueLightWithSize(15)
        pointsValueLabel.font = helveticaNeueLightWithSize(15)
    }
}
