//
//  ProfileViewController.swift
//  IMHO
//
//  Created by MacBook Pro on 12/3/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate {

    var firstTimeLoad:Bool = false
    
    var delegate:AppDelegate = (UIApplication.sharedApplication().delegate! as! AppDelegate)

    var userMissions:NSMutableArray = NSMutableArray()
    
    //Global Variable to set before view load.
    var tempUser: UserModel?
    
    var contentArray:NSMutableArray = NSMutableArray()
    
    @IBOutlet weak var profileCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
                
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        loadData()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - ProfileViewController methods

    
    func setUser(user:UserModel) {
        tempUser = user
    }
    
    func loadData() {
        
        delegate.loggedUser.getClossedMissionsWithSuccess({ (array) -> () in
            self.userMissions = array.mutableCopy() as! NSMutableArray
            self.profileCollectionView.reloadData()
            
        }, errorBlock: { (error) -> () in
            
        })
    }
    
    // MARK: - UICollectionViewDataSource methods
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.userMissions.count + 1
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        if (indexPath.item == 0) {
            let cell:ProfileCollectionCell = collectionView.dequeueReusableCellWithReuseIdentifier("ProfileCollectionCell", forIndexPath: indexPath) as! ProfileCollectionCell
        //    let user = delegate.loggedUser
            cell.userNameLabel.text = delegate.loggedUser.name!
            if(delegate.loggedUser.profileImageURL != nil){
                delegate.loggedUser.getProfileImageWithSuccess({ (image) -> () in
                    cell.profileImageView.image = image
                    }, errorBlock: { (error) -> () in
                        print("Error imagen perfil")
                })
                
            }
            
            delegate.getRoundImageFromImage(cell.profileImageView)
            delegate.getRoundImageFromImage(cell.borderImageView)

            cell.missionsValueLabel.text = String(format: "%d",userMissions.count)
            cell.friendsValueLabel.text = String(format: "%d",delegate.loggedUser.friends!.count)
            cell.pointsValueLabel.text = String(format: "%d",delegate.loggedUser.rankPoints)
            
            cell.setup()
            
            let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .Light)) as UIVisualEffectView
            
            visualEffectView.frame = cell.backgroundImageView.bounds
           
            //TODO arreglar este FastFix
            if (!firstTimeLoad){
                cell.backgroundImageView.addSubview(visualEffectView)
                firstTimeLoad = true
            }
            
            return cell
        } else {
            
            let cell:HomeCollectionCell = collectionView.dequeueReusableCellWithReuseIdentifier("ProfileMisionCell", forIndexPath: indexPath) as! HomeCollectionCell
            
            let index:Int = indexPath.item - 1
            let mission:MissionModel =  userMissions.objectAtIndex(index) as! MissionModel
            
            if (mission.images!.count > 1) {
                
                let firstImage:ImageModel = mission.images!.firstObject as! ImageModel
                
                firstImage.getImageWithBlock({ (image) -> () in
                    UIView.animateWithDuration(0.8, animations: { () -> Void in
                        cell.firstImageView.image = image
                        cell.firstImageView.layer.masksToBounds = true
                        cell.firstImageView.contentMode = .ScaleAspectFill
                    })
                })
                
                let secondImage:ImageModel = mission.images!.objectAtIndex(1) as! ImageModel
                
                secondImage.getImageWithBlock({ (image) -> () in
                    UIView.animateWithDuration(0.8, animations: { () -> Void in
                        cell.secondImageView.image = image
                        cell.secondImageView.layer.masksToBounds = true
                        cell.secondImageView.contentMode = .ScaleAspectFill
                    })
                })
                
                cell.firstImageView.hidden = false
                cell.secondImageView.hidden = false
                cell.missionImageView.hidden = true
                cell.separatorImageView.hidden = false
                
            } else {
                let image:ImageModel = mission.images!.firstObject as! ImageModel
                
                image.getImageWithBlock({ (image) -> () in
                    UIView.animateWithDuration(0.8, animations: { () -> Void in
                        cell.missionImageView.image = image
                        cell.missionImageView.layer.masksToBounds = true
                        cell.missionImageView.contentMode = .ScaleAspectFill
                    })
                })
                
                cell.firstImageView.hidden = true
                cell.secondImageView.hidden = true
                cell.missionImageView.hidden = false
                cell.separatorImageView.hidden = true
            }
            
            return cell
        }
    }
    
    //MARK: - UICollectionViewFlowLayoutDelegate methods
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if (indexPath.item == 0) {
            return CGSizeMake(collectionView.frame.size.width, 250)
        } else {
            let cellWidth:CGFloat = collectionView.frame.size.width/3
            
            return CGSizeMake(cellWidth, cellWidth)
        }
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        if (indexPath.item > 0) {
            
            let detailVC:HomeMissionDetailsController = self.delegate.homeStoryboard.instantiateViewControllerWithIdentifier("MissionDetails") as! HomeMissionDetailsController
            
            let index:Int = indexPath.item - 1
            let mission:MissionModel =  userMissions.objectAtIndex(index) as! MissionModel
            
            detailVC.isMissionLoaded = true
            detailVC.selectedMission = mission
            
            self.navigationController!.pushViewController(detailVC, animated: true)
        }
        
    }
    
}