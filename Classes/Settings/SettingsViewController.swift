//
//  SettingsViewController.swift
//  IMHO
//
//  Created by Rafael Zamora on 3/8/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var delegate:AppDelegate = (UIApplication.sharedApplication().delegate! as! AppDelegate)
    
    var contentArray:NSMutableArray = NSMutableArray()

    
    @IBOutlet weak var settingsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Setup additional text
        localize()
        
        // Insert settings to the tableview
        createSettings()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - SettingsViewController methods
    
    func createSettings() {
        
        let profileSetting:SettingData = SettingData(aName: "Profile", anIcon: "SettingsProfileIcon", aCellIdentifier: "settingsIndicatorCell")
        
        self.contentArray.addObject(profileSetting)
        
        let notificationSetting:SettingData = SettingData(aName: "Notifications", anIcon: "SettingsNotificationIcon", aCellIdentifier: "settingsIndicatorCell")
        
        self.contentArray.addObject(notificationSetting)
        
        let autosaveSetting:SettingData = SettingData(aName: "Auto-Save Pictures", anIcon: "SettingsPictureIcon", aCellIdentifier: "settingsSwitchCell")
        
        self.contentArray.addObject(autosaveSetting)
        
        let socialSetting:SettingData = SettingData(aName: "Social Connect", anIcon: "SettingsSocialIcon", aCellIdentifier: "settingsIndicatorCell")
        
        self.contentArray.addObject(socialSetting)
        
        let aboutSetting:SettingData = SettingData(aName: "About", anIcon: "SettingsInfoIcon", aCellIdentifier: "settingsIndicatorCell")
        
        self.contentArray.addObject(aboutSetting)
        
        let termsSetting:SettingData = SettingData(aName: "Terms & Conditions", anIcon: "SettingsTermsIcon", aCellIdentifier: "settingsIndicatorCell")
        
        self.contentArray.addObject(termsSetting)
        
        let logoutSetting:SettingData = SettingData(aName: "Log Out", anIcon: "SettingsLogOutIcon", aCellIdentifier: "settingsIndicatorCell")
        
        self.contentArray.addObject(logoutSetting)
    }
    
    func localize() {
        
        self.title = getLocalizedStringForKey("settingsScreenTitle")
    }

    // MARK: - UITableViewDataSource methods
    
    // Estos son los 3 metodos obligatorios para programar una tabla
    
    // Numero de secciones en la tabla (letras del abecedario)
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    // Numero de filas en cada seccion
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contentArray.count
    }
    // Vista de la celda
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let setting:SettingData = self.contentArray.objectAtIndex(indexPath.row) as! SettingData
        
        let cell:SettingsTableCell = tableView.dequeueReusableCellWithIdentifier(setting.cellIdentifier!) as! SettingsTableCell
        
        cell.titleLabel.text = setting.name!
        cell.titleLabel.font = helveticaNeueLightWithSize(17)
        cell.titleLabel.textColor = kAppTextColor
        
        cell.iconImageView.image = UIImage(named: setting.icon!)
        
        cell.separatorview.backgroundColor = kAppSecondaryColor
        
        if (cell.settingsSwitch != nil) {
            cell.settingsSwitch.transform = CGAffineTransformMakeScale(0.7, 0.7)
            cell.settingsSwitch.onTintColor = kAppSecondaryColor
        }
        
        return cell
    }
    
    // MARK: UITableViewDelegate methods
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
}
