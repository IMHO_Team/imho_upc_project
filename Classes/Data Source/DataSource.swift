//
//  DataSource.swift
//  IMHO
//
//  Created by Rafael Zamora on 4/11/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import UIKit
import Parse
import Bolts

class DataSource {
    
    
    class var sharedInstance: DataSource {
        struct Singleton {
        
            static let instance = DataSource()
        }
        
        return Singleton.instance
    }
    
    init() {
        
    }
    
    // MARK: - Auth functions
    
    /**
    Login user with facebook
    
    - parameter successBlock: Block ejecuted when user did login
    */
    func facebookLoginWithSuccess(successBlock:(user :UserModel) ->(), errorBlock:(error:NSError) ->()) {
        
        PFFacebookUtils.logInInBackgroundWithReadPermissions(["email","user_friends","public_profile"], block: { (user, error) -> Void in
            if (error == nil) {
              //  var delegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                
                //TODO - Chequear si el user no es nul
                let loggedUser:UserModel = UserModel(parseUser: user!)
                
                self.getFacebookDataForUser(loggedUser, withSuccess: { (user) -> () in
                    
                    let delegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                
                    delegate.loggedUser = user
                    
                     self.getFriendsListForUser(delegate.loggedUser, withSuccessBlock: { (friendList) -> () in
                     
                    //Adding the friends Parse user list get from the facebook friends.
                    // TODO: Make this calling allways after application start 
                     delegate.loggedUser.friends = friendList
                        
                     })
                    
                    self.getProfilePictureForLoggedUserWithSuccess({ () -> () in
                        
                    })
                    
                    delegate.loggedUser.getFriends({ (friends) -> () in
                        
                    })
                    
                    successBlock(user: loggedUser)
                    
                }, errorBlock: { (error) -> () in
                    
                })
                
            } else {
                print("error \(error!.localizedDescription)")
                errorBlock(error: error!)
            }
        })
    }
    
    /**
    Get additional information from Facebook
    
    - parameteruser: The user needed to save data
    - parametersuccessBlock: block executed when succeeded
    - parametererrorBlock: block executed when not succeeded
    */
    func getFacebookDataForUser(user:UserModel, withSuccess successBlock:(user: UserModel) -> (), errorBlock:(error:NSError) -> ()) {
        
        if ((FBSDKAccessToken.currentAccessToken()) != nil) {
            let fbGraphRequest:FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: nil)
            
            fbGraphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
                
                if (error == nil) {
                    print("\(result)")
                    user.mail = (result as! NSDictionary).objectForKey("email") as? String
                    user.name = (result as! NSDictionary).objectForKey("name") as? String
                    if ((result as! NSDictionary).objectForKey("gender") != nil){
                        user.gender = (result as! NSDictionary).objectForKey("gender") as? String
                    }else{
                        user.gender = ""
                    }
                    user.facebookId = (result as! NSDictionary).objectForKey("id") as? String
                    
                    
                    user.saveInformationWithSuccess({ () -> () in
                        successBlock(user: user)
                        }, errorBlock: { (error) -> () in
                            errorBlock(error: error)
                    })
                    
                    
                } else {
                    errorBlock(error: error)
                }
            })
        }
    }

    
    /**
    Get profile image from the Facebook logged user.
    
    - parameter
    */
    func getProfilePictureForLoggedUserWithSuccess(successBlock:() ->()){
       
        let fbuserid = FBSDKAccessToken.currentAccessToken().userID
        //Get users info (email,firstname,lastname,username)
        let url = NSURL(string: "https://graph.facebook.com/\(fbuserid)/picture?type=large")
        let urlRequest = NSURLRequest(URL: url!)
        
        
        
        NSURLConnection.sendAsynchronousRequest(urlRequest, queue: NSOperationQueue.mainQueue()) {(response:NSURLResponse?, data:NSData?, error:NSError?) -> Void in
            
            if (error == nil) {
               let image = UIImage(data: data!)
                let delegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                delegate.loggedUser.profileImage = image
                delegate.loggedUser.pUser!.setObject(url!.absoluteString, forKey: kParseVariableUserProfileImage)
                delegate.loggedUser.pUser!.saveInBackgroundWithBlock({ (success, error) -> Void in
                    if (success) {
                        successBlock()
                    }
                })
            }
            
            
        }
    }
    
    
    
    /**
    Get additional information from the Facebook logged user.
    
    - parameter
    */
    func getLoggedUserFacebookInformation (user :UserModel){
        //code
        let loggedUserFBProfile:FBSDKProfile = FBSDKProfile()
        user.name = loggedUserFBProfile.firstName
        user.identifier = loggedUserFBProfile.userID
        
        print("\(loggedUserFBProfile.name)")
        
        
    }
    
    
    
    // MARK: - Mission functions
    
    /**
    Get missions with status 'running' not answered by user
    
    - parameter user: The single user
    - parameter successBlock: Block ejecuted when missions returned
    */
    func getNewMissionsForUser(user:UserModel, successBlock:(array :NSMutableArray) -> ()) {
        
        //[{"__type":"Pointer","className":"_User","objectId":"rHkakoattL"}]
        
        let answerQuery:PFQuery = PFQuery(className: kParseClassAnswer)
        answerQuery.whereKey(kParseVariableAnswerUser, equalTo: user.pUser!)
        answerQuery.includeKey(kParseVariableAnswerMission)
        
        answerQuery.findObjectsInBackgroundWithBlock { (answerResults: [AnyObject]?, error:NSError?) -> Void in
            
            if (error == nil) {
                
                let missionsIds:NSMutableArray = NSMutableArray()
                
                for object in answerResults! {
                    
                    let answer:PFObject = object as! PFObject
                    let mission:PFObject = answer.objectForKey(kParseVariableAnswerMission)! as! PFObject
                    missionsIds.addObject(mission.objectId!)
                }
                
                let missionQuery:PFQuery = PFQuery(className: kParseClassMission)
                
                missionQuery.whereKey(kParseVariableMissionSingleUsers, equalTo: user.pUser!)
                missionQuery.whereKey(kParseVariableMissionStatus, equalTo: kParseValueMissionStatusRunning)
                missionQuery.whereKey(kParseVariableMissionObjectId, notContainedIn: missionsIds as [AnyObject])
                missionQuery.includeKey(kParseVariableMissionImage1)
                missionQuery.includeKey(kParseVariableMissionImage2)
                missionQuery.includeKey(kParseVariableMissionSingleUsers)
                missionQuery.includeKey(kParseVariableMissionUser)
                missionQuery.includeKey(kParseVariableMissionAnswers)
                missionQuery.includeKey(kParseVariableAnswerUser)
                missionQuery.includeKey(kParseVariableAnswerSingleAnswer)
                missionQuery.orderByDescending(kParseVariableMissionCreatedAt)
                missionQuery.findObjectsInBackgroundWithBlock {(results, error) -> Void in
                    
                    if (error == nil) {
                        
                        let missions:NSMutableArray = NSMutableArray()
                        print("\(results!.count)")
                        for object in results! {
                            missions.addObject(MissionModel(parseObject: object as! PFObject))
                        }
                        
                        successBlock(array: missions)
                        
                    } else {
                        print("error \(error!.localizedDescription)")
                    }
                }
            }
        }
    }
    
    /**
    Add answer to mission
    
    - parameter mission: The single mission
    - parameter successBlock: Block executed when answer added
    */
    func addAnswer(answer:AnswerModel, toMission mission:MissionModel, withSuccessBlock successBlock:(succeeded :Bool) -> ()) {
        
        var answersArray:NSMutableArray = NSMutableArray()
        
        if (mission.parseMission!.objectForKey(kParseVariableMissionAnswers) != nil) {
            answersArray = (mission.parseMission!.objectForKey(kParseVariableMissionAnswers) as! NSArray).mutableCopy() as! NSMutableArray
        }
        
        answersArray.addObject(answer.parseAnswer!)
        
        mission.parseMission!.setObject(answersArray, forKey: kParseVariableMissionAnswers)
        
        mission.parseMission!.saveInBackgroundWithBlock { (succeeded, error) -> Void in
            successBlock(succeeded: succeeded)
        }
        
    }
    
    /**
    Add Mission
    - parameter mission: The single mission
    - parameter successBlock: Block ejecuted when mission added
    - parameter successBlock: Block ejecuted when error occurred
    */
    func addMission(mission:MissionModel, withSuccessBlock successBlock:() -> (), andErrorBlock errorBlock:(error:NSError)->()) {
        
        mission.parseMissionFromMissionWithSuccess({ () -> () in
            
            mission.parseMission!.saveInBackgroundWithBlock { (succeeded, error) -> Void in
                if(error==nil){
                    successBlock()
                }else{
                    print("\(error!.description)")
                    errorBlock(error: error!)
                }
            }
            
        }, errorBlock: { (error) -> () in
            
            errorBlock(error: error)
        })
    }
    
    /**
    Get missions with status 'running' already answered by user

    - parameter user: The single user
    */
    func getCurrentMissionToUser(user:UserModel, successBlock:(array:NSMutableArray) -> ()) {

        let answerQuery:PFQuery = PFQuery(className: kParseClassAnswer)
        answerQuery.whereKey(kParseVariableAnswerUser, equalTo: user.pUser!)
        answerQuery.includeKey(kParseVariableAnswerMission)
        
        answerQuery.findObjectsInBackgroundWithBlock { (answerResults: [AnyObject]?, error:NSError?) -> Void in
            
            if (error == nil) {
                
                let missionsIds:NSMutableArray = NSMutableArray()
                
                for object in answerResults! {
                    
                    let answer:PFObject = object as! PFObject
                    let mission:PFObject = answer.objectForKey(kParseVariableAnswerMission)! as! PFObject
                    missionsIds.addObject(mission.objectId!)
                }
                
                let missionQuery:PFQuery = PFQuery(className: kParseClassMission)
                
                missionQuery.whereKey(kParseVariableMissionSingleUsers, equalTo: user.pUser!)
                missionQuery.whereKey(kParseVariableMissionStatus, equalTo: kParseValueMissionStatusRunning)
                missionQuery.whereKey(kParseVariableMissionObjectId, containedIn: missionsIds as [AnyObject])
                
                missionQuery.includeKey(kParseVariableMissionImage1)
                missionQuery.includeKey(kParseVariableMissionImage2)
                missionQuery.includeKey(kParseVariableMissionSingleUsers)
                missionQuery.includeKey(kParseVariableMissionUser)
                missionQuery.includeKey(kParseVariableMissionAnswers)
                missionQuery.includeKey(kParseVariableAnswerUser)
                missionQuery.includeKey(kParseVariableAnswerSingleAnswer)
                missionQuery.orderByDescending(kParseVariableMissionCreatedAt)
                missionQuery.findObjectsInBackgroundWithBlock {(results, error) -> Void in
                    
                    if (error == nil) {
                        
                        let missions:NSMutableArray = NSMutableArray()
                        
                        for object in results! {
                            missions.addObject(MissionModel(parseObject: object as! PFObject))
                        }
                        
                        successBlock(array: missions)
                        
                    } else {
                        print("error \(error!.localizedDescription)")
                    }
                }
            }
        }
    }
    
    /**
    Get missions with the status 'running'
    
    - parameter image,: Image to save
    - parameter successBlock,: Success block executed when finished
    - parameter errorBlock,: Block executed when operation didn't succeed
    */
    func getCurrentMissionFromUser(user:UserModel, successBlock:(array:NSMutableArray) -> ()) {
        
        
        let missionQuery:PFQuery = PFQuery(className: kParseClassMission)
        
        missionQuery.whereKey(kParseVariableMissionStatus, equalTo: kParseValueMissionStatusRunning)
        missionQuery.whereKey(kParseVariableMissionUser, equalTo: user.pUser!)
        missionQuery.includeKey(kParseVariableMissionImage1)
        missionQuery.includeKey(kParseVariableMissionImage2)
        missionQuery.includeKey(kParseVariableMissionSingleUsers)
        missionQuery.includeKey(kParseVariableMissionUser)
        missionQuery.includeKey(kParseVariableMissionAnswers)
        missionQuery.includeKey(kParseVariableAnswerUser)
        missionQuery.includeKey(kParseVariableAnswerSingleAnswer)
        missionQuery.orderByDescending(kParseVariableMissionCreatedAt)
        missionQuery.findObjectsInBackgroundWithBlock {(results, error) -> Void in
            
            if (error == nil) {
                
                let missions:NSMutableArray = NSMutableArray()
                
                for object in results! {
                    missions.addObject(MissionModel(parseObject: object as! PFObject))
                }
                
                successBlock(array: missions)
                
            } else {
                print("error \(error!.localizedDescription)")
            }
        }
    }
    
    /**
    Get public missions for a category
    
    - parameter category,: category
    - parameter successBlock,: Success block executed when finished
    - parameter errorBlock,: Block executed when operation didn't succeed
    */
    func getPublicMissionsForCategory(category:CategoryModel, successBlock:(array:NSMutableArray) -> ()) {
        
        let delegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let answerQuery:PFQuery = PFQuery(className: kParseClassAnswer)
        answerQuery.whereKey(kParseVariableAnswerUser, equalTo: delegate.loggedUser.pUser!)
        answerQuery.includeKey(kParseVariableAnswerMission)
        
        answerQuery.findObjectsInBackgroundWithBlock { (answerResults, error) -> Void in
            
            if (error == nil) {
                
                let missionsIds:NSMutableArray = NSMutableArray()
                
                for object in answerResults! {
                    
                    let answer:PFObject = object as! PFObject
                    let mission:PFObject = answer.objectForKey(kParseVariableAnswerMission)! as! PFObject
                    missionsIds.addObject(mission.objectId!)
                }
                
                let missionQuery:PFQuery = PFQuery(className: kParseClassMission)
                missionQuery.whereKey(kParseVariableMissionCategory, equalTo: category.parseCategory!)
                missionQuery.whereKey(kParseVariableMissionIsPrivate, equalTo: false)
                missionQuery.whereKey(kParseVariableMissionUser, notEqualTo: delegate.loggedUser.pUser!)
                missionQuery.whereKey(kParseVariableMissionObjectId, notContainedIn: missionsIds as [AnyObject])
                missionQuery.includeKey(kParseVariableMissionImage1)
                missionQuery.includeKey(kParseVariableMissionImage2)
                missionQuery.includeKey(kParseVariableMissionSingleUsers)
                missionQuery.includeKey(kParseVariableMissionUser)
                missionQuery.includeKey(kParseVariableMissionAnswers)
                missionQuery.includeKey(kParseVariableAnswerUser)
                missionQuery.includeKey(kParseVariableAnswerSingleAnswer)
                missionQuery.orderByDescending(kParseVariableMissionCreatedAt)
                missionQuery.findObjectsInBackgroundWithBlock({ (missionResults: [AnyObject]?, error:NSError?) -> Void in
                    
                    if (error == nil) {
                        
                        let missions:NSMutableArray = NSMutableArray()
                        
                        for object in missionResults! {
                            
                            missions.addObject(MissionModel(parseObject: object as! PFObject))
                        }
                        
                        successBlock(array: missions)
                    }
                })
            }
        }
    }
    
    /**
    Add users to singleUsers field on mission
    
    - parameter mission,: Mission to edit
    - parameter successBlock,: Success block executed when finished
    - parameter errorBlock,: Block executed when operation didn't succeed
    */
    func forwardMission(mission:MissionModel, toUsers users:NSMutableArray, withSuccess successBlock:() ->(), errorBlock:(error:NSError) ->()) {
        
        for user in users {
            mission.parseMission!.addUniqueObject((user as! UserModel).pUser!, forKey: kParseVariableMissionSingleUsers)
        }
        
        mission.parseMission!.saveInBackgroundWithBlock { (succeeded, error) -> Void in
            
            if (succeeded) {
                
                mission.singleUsers = NSMutableArray()
                
                for object in (mission.parseMission!.objectForKey(kParseVariableMissionSingleUsers) as! NSArray) {
                    
                    mission.singleUsers!.addObject(UserModel(parseUser: (object as! PFUser)))
                }
                
                successBlock()
            } else {
                errorBlock(error: error!)
            }
        }
        
        
    }
    
    /**
    Get user's mission with status 'closed'
    
    - parameter user,: User to request missions
    - parameter successBlock,: Success block executed when finished
    - parameter errorBlock,: Block executed when operation didn't succeed
    */
    func getClosedMissionsForUser(user:UserModel, withSuccess successBlock:(results:NSMutableArray) -> (), errorBlock:(error:NSError) -> ()) {
        
        let missionQuery:PFQuery = PFQuery(className: kParseClassMission)
        missionQuery.whereKey(kParseVariableMissionUser, equalTo: user.pUser!)
        missionQuery.whereKey(kParseVariableMissionStatus, equalTo: kParseValueMissionStatusClosed)
        missionQuery.includeKey(kParseVariableMissionImage1)
        missionQuery.includeKey(kParseVariableMissionImage2)
        missionQuery.includeKey(kParseVariableMissionSingleUsers)
        missionQuery.includeKey(kParseVariableMissionUser)
        missionQuery.includeKey(kParseVariableMissionAnswers)
        missionQuery.includeKey(kParseVariableAnswerUser)
        missionQuery.includeKey(kParseVariableAnswerSingleAnswer)
        missionQuery.orderByDescending(kParseVariableMissionCreatedAt)
        missionQuery.findObjectsInBackgroundWithBlock { (results, error) -> Void in
            
            if (error == nil) {
                
                let missionsArray:NSMutableArray = NSMutableArray()
                
                for object in (results! as NSArray) {
                
                    missionsArray.addObject(MissionModel(parseObject: (object as! PFObject)))
                }
                
                successBlock(results: missionsArray)
                
            } else {
                errorBlock(error: error!)
            }
        }
    }
    
    /**
    Set mission status to 'closed'
    
    - parameter image,: Mission to close
    - parameter successBlock,: Success block executed when finished
    - parameter errorBlock,: Block executed when operation didn't succeed
    */
    func closeMission(mission:MissionModel, withSuccess successBlock:() -> (), errorBlock:(error:NSError) -> ()) {
        
        mission.parseMission!.setObject(kParseValueMissionStatusClosed, forKey: kParseVariableMissionStatus)
        mission.parseMission!.saveInBackgroundWithBlock { (succeeded, error) -> Void in
            
            if (succeeded) {
                successBlock()
            } else {
                errorBlock(error: error!)
            }
        }
    }
    
    /**
    Fetch mission data
    
    - parameter identifier,: Mission identifier to fetch
    - parameter successBlock,: Success block executed when finished
    - parameter errorBlock,: Block executed when operation didn't succeed
    */
    func fetchMission(identifier:String, withSuccess successBlock:(mission:MissionModel) -> (), errorBlock:(error:NSError) -> ()) {
        
        let missionQuery:PFQuery = PFQuery(className: kParseClassMission)
        missionQuery.includeKey(kParseVariableMissionImage1)
        missionQuery.includeKey(kParseVariableMissionImage2)
        missionQuery.includeKey(kParseVariableMissionSingleUsers)
        missionQuery.includeKey(kParseVariableMissionUser)
        missionQuery.includeKey(kParseVariableMissionAnswers)
        missionQuery.includeKey(kParseVariableAnswerUser)
        missionQuery.includeKey(kParseVariableAnswerSingleAnswer)
        
        missionQuery.getObjectInBackgroundWithId(identifier, block: { (result, error) -> Void in
          
            if (error == nil) {
                successBlock(mission: MissionModel(parseObject: result!))
            } else {
                errorBlock(error: error!)
            }
        })
    }
    
    // MARK: - Answer functions
    
    /**
    Save new answer
    
    - parameter successBlock: Block executed when answer created
    */
    func saveAnswer(answer:AnswerModel, withSuccessBlock successBlock:(answer :AnswerModel) -> ()) {
        
        let parseAnswer:PFObject = parseAnswerFromAnswer(answer)
        
        parseAnswer.saveInBackgroundWithBlock { (succeeded, error) -> Void in
            
            if (succeeded) {
                successBlock(answer: AnswerModel(parseObject: parseAnswer))
            }
        }
    }
    
    /**
    Create PFObject from model answer
    
    - parameter successBlock: Block executed when answer created
    */
    func parseAnswerFromAnswer(answer:AnswerModel) -> PFObject {
        
        let parseAnswer:PFObject = PFObject(className: kParseClassAnswer)
        
        parseAnswer.setObject(answer.user!.pUser!, forKey: kParseVariableAnswerUser)
        parseAnswer.setObject(answer.mission!.parseMission!, forKey: kParseVariableAnswerMission)
        parseAnswer.setObject(answer.comment!, forKey: kParseVariableAnswerComment)
        parseAnswer.setObject(answer.selectedImage!.parseImage!, forKey: kParseVariableAnswerSelectedImage)
        parseAnswer.setObject(answer.singleAnswer!, forKey: kParseVariableAnswerSingleAnswer)
        parseAnswer.setObject(answer.points!, forKey: kParseVariableAnswerPoints)
        
        return parseAnswer
    }
    
    /**
        Adds the points of an answer
    
    */
    func AddPointsToUserWhenAnswer(user:UserModel, withSuccessBlock successBlock:(user :UserModel) -> ()) {
        
        let userUpdated:PFUser = user.getParseUser()
        
        userUpdated.incrementKey(kParseVariableUserRankPoints, byAmount: 2)
            
        userUpdated.saveInBackgroundWithBlock { (succeeded, error) -> Void in
                
            if (succeeded) {
                successBlock(user:UserModel(parseUser: userUpdated))
            }
        }
    }
    
    /**
    Get answers from a mission
    
    - parameter user: The mission
    */
    func getAnswersFromMission(mission:MissionModel, successBlock:(array:NSMutableArray) -> ()) {
        
        let answerQuery:PFQuery = PFQuery(className: kParseClassAnswer)
        answerQuery.whereKey(kParseVariableAnswerMission, equalTo: mission.parseMission!)
        answerQuery.includeKey(kParseVariableAnswerMission)
        answerQuery.includeKey(kParseVariableAnswerUser)
//        answerQuery.includeKey(kParseVariableAnswerSelectedImage)
       
        answerQuery.findObjectsInBackgroundWithBlock { (answerResults: [AnyObject]?, error:NSError?) -> Void in
            
            if (error == nil) {
                
                let answerArray:NSMutableArray = NSMutableArray()
                
                for object in answerResults! {
                    
                    answerArray.addObject(AnswerModel(parseObject: (object as! PFObject), simple: true))
                }
                
                successBlock(array: answerArray)
            }
        }
    }
    
    // MARK: - Category functions
    
    /**
    Returns the category list
    
    - parameter successBlock: Block executed when list returned
    */
    func getCategoryListWithSuccessBlock(successBlock:(categories:NSMutableArray) -> ()) {
        
        let categoryQuery:PFQuery = PFQuery(className: kParseClassCategory)
        categoryQuery.includeKey(kParseVariableCategoryImage)
        
        categoryQuery.findObjectsInBackgroundWithBlock { (results: [AnyObject]?, error:NSError?) -> Void in
            
            if (error == nil) {
                
                let categoryArray:NSMutableArray = NSMutableArray()
                
                for object in results! {
                
                    categoryArray.addObject(CategoryModel(parseObject: object as! PFObject))
                }
                
                successBlock(categories: categoryArray)
            }
        }
        
    }
    
    
    
    // MARK: - Users functions
    /**
    Save user information to parse

    - parameteruser: User to save
    - parametersuccessBlock: block executed when succeeded
    - parametererrorBlock: block executed when not succeeded
    */    
    func saveUser(user:UserModel, withSuccess successBlock:() -> (), errorBlock:(error:NSError) -> ()) {
        
        user.getParseUser().saveInBackgroundWithBlock { (succeeded, error) -> Void in
            if (succeeded) {
                successBlock()
            } else {
                errorBlock(error: error!)
            }
        }
    }

    
    /**
    Returns the friends list from a specifi user
    
    - parameter successBlock: Block executed when friends list returned
    */
    func getFriendsListForUser(user:UserModel, withSuccessBlock successBlock:(friendList :NSMutableArray) -> ()){
        
        print("I'm in the getFriendsListForUser Method!")
        
    //    var parseUserFriends: NSMutableArray = NSMutableArray()
    //    var parseFriend: PFObject
        
       getFacebookFriends(user, withSuccessBlock: { (friendList) -> () in
        
        //The friendlist contains all the facebook friends ID+name.
        
        let friendIds: NSMutableArray = NSMutableArray(capacity: friendList.count)
        
        // Create a list of friends' Facebook IDs
        for friendObject in (friendList as NSArray) {
            
            let friendId = (friendObject as! NSDictionary).objectForKey("id") as? String
            
            friendIds.addObject(friendId!)
        }
        
        let friendQuery: PFQuery = PFUser.query()!
        friendQuery.whereKey("facebookId", containedIn: friendIds as [AnyObject])
        
        friendQuery.findObjectsInBackgroundWithBlock({ (friendPUsers, error) -> Void in
            
            if (error == nil){
                
                let friendUsersArray:NSMutableArray = NSMutableArray()
                
                for object in friendPUsers! {
                    
                    friendUsersArray.addObject(UserModel(parseUser: object as! PFUser))
                }
                
                let delegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                
                delegate.loggedUser.friends = friendUsersArray
                
                //Call the method to add all the friend to the user
                self.addFriendsToCurrentUser(user, parseFriends: friendUsersArray, withSuccessBlock: { () -> () in

                    successBlock(friendList: friendUsersArray)
                
                })
                
                print("friendUsersArray --> \(friendUsersArray)")
                
                
                
            }else{
                print("User not found! Sorry.")
            }
            
        })
        
        })
    }

    

        
        
    /**
    Returns the facebook friends list from a specifi user
    
    - parameter successBlock: Block executed when friends list returned
    */
    func getFacebookFriends (user:UserModel, withSuccessBlock successBlock:(friendList :NSMutableArray) -> ()){
        
        let fbRequest = FBSDKGraphRequest(graphPath:"/me/friends", parameters: nil);
        fbRequest.startWithCompletionHandler { (connection : FBSDKGraphRequestConnection!, result : AnyObject!, error : NSError!) -> Void in
            var friendsFBarray :NSMutableArray = NSMutableArray()
            if error == nil {
                //println("Friends result are : \(result)")
                if (result != nil) {
                    friendsFBarray  = result.objectForKey("data") as! NSMutableArray
                }
                //println("Friends array are : \(friendsFBarray)")
            } else {
                //println("Error Getting Friends \(error)");
            }
            successBlock(friendList: friendsFBarray)
        }
    }
    
    
    
    /**
    Method to add the parameter friend to the Parse User friends list.
    
    - parameter Current: user, friend to add and successBlock Block executed when finished
    */
    func  addFriendsToCurrentUser(user:UserModel, parseFriends:NSMutableArray, withSuccessBlock successBlock:() -> ()){
        
        for friend in parseFriends{
        
            //Adding the friend to the user friend list if the user does not exist. In other case, nothing happens.
            user.pUser!.addUniqueObject((friend as! UserModel).pUser!, forKey: kParseVariableUserFriends)
        
        }
        
        print("I've added \(parseFriends.count) users.")
    
        user.pUser?.saveInBackgroundWithBlock({ (succed, error) -> Void in
            
            if error == nil {
                
                successBlock()
            
            }
        })
    }
    
    /**
    Returns the friends list from a specifi user
    
    - parameter successBlock: Block executed when friends list returned
    */
    func getUserFriendsSortByPoints(user:UserModel, withSuccessBlock successBlock:(friendList :NSMutableArray) -> ()){
        
        print("I'm in the getUserFriendsSortByPoints Method!")
    
        
        let friendsQuery: PFQuery = PFUser.query()!
        friendsQuery.includeKey(kParseVariableUserFriends)
        friendsQuery.orderByDescending(kParseVariableUserRankPoints)
        friendsQuery.findObjectsInBackgroundWithBlock { (parseUsersList, error) -> Void in
            
             if (error == nil) {
            
                let userFriends:NSMutableArray = NSMutableArray()
                
                for object in parseUsersList! {
                    
                    userFriends.addObject(UserModel(parseUser: object as! PFUser))
                }

                successBlock(friendList: userFriends)
                
            }
        }
    }

    
    // MARK: - Images functions
    /**
    Save Image Object in Parse with fileName field
    
    - parameter image,: Image to save
    - parameter successBlock,: Success block executed when finished
    - parameter errorBlock,: Block executed when operation didn't succeed
    */
    func saveImage(image:ImageModel, withSuccess successBlock:() -> (), errorBlock:(error:NSError) ->()) {
        
        image.parseImage = PFObject(className: kParseClassImage)
        image.parseImage!.setObject(image.fileName!, forKey: kParseVariableImageFileName)
        image.parseImage!.saveInBackgroundWithBlock { (succeeded, error) -> Void in
            if (succeeded) {
                successBlock()
            } else {
                errorBlock(error: error!)
            }
        }
        
    }
    
    // MARK: - Push notifications
    
    func sendPushNotificationToUser(user:UserModel, withMessage message:String, andData data:NSMutableDictionary) {
        
        let push:PFPush = PFPush()
        data.setObject(message, forKey: "alert")
        push.setData(data as [NSObject : AnyObject])
        push.setChannels([kChannelPrefix+user.identifier!])
        push.sendPushInBackground()
    }
    
    func notifyNewMissionToUser(user:UserModel) {
        
        let delegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let message:String = delegate.loggedUser.name! + getLocalizedStringForKey("notificationMissionSent")
        
        sendPushNotificationToUser(user, withMessage: message, andData: NSMutableDictionary(object: NSNumber(integer: 2), forKey: "notificationid"))
    }
    
    func notifyNewAnswerToUser(user:UserModel, withAnswer answer:Bool, andMission mission:MissionModel) {
        
        let delegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        var message:String = delegate.loggedUser.name! + getLocalizedStringForKey("notificationAnswerSent")
        
        if (answer) {
            message = message + " 👍"
        } else {
            message = message + " 👎"
        }
        
        sendPushNotificationToUser(user, withMessage: message, andData: NSMutableDictionary(objects: [NSNumber(integer: 3),mission.identifier!], forKeys: ["notificationid","missionid"]))
    }
    
    
    
    
    // MARK: - Parse Cloud functions
    
    func checkInCloudIfLastAnswer(missionID:String) {
        print("I'm on checkInCloudIfLastAnswer method. The mission ID is : \(missionID)")
        PFCloud.callFunctionInBackground(kParseCloudFuncionLastAnswer, withParameters: ["mission":missionID]){
            (results, error) -> Void in
            
        }
    }
    
}
