//
//  UserModel.swift
//  IMHO
//
//  Created by MacBook Pro on 14/3/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import UIKit
import Parse

class UserModel: NSObject {
    
    var identifier:String?
    var facebookId:String?
    var name:String?
    var gender:String?
    var mail:String?
    var birthDate:NSDate?
    var location:String?
    var profileImage:UIImage?
    var profileImageURL:String?
    var friends:NSMutableArray?
    var pUser:PFUser?
    var clossedMissions:NSMutableArray?
    var rankPoints:Int = 0
    
    init(aName: String, aGender:String, aMail:String, aBirthDate:NSDate, aLocation:String) {
        super.init()
        
        name = aName
        gender = aGender
        mail = aMail
        birthDate = aBirthDate
        location = aLocation
    }
   
    init(aName: String, aGender:String, aMail:String, aBirthDate:NSDate, aLocation:String, aProfileImage:UIImage) {
        super.init()
        
        name = aName
        gender = aGender
        mail = aMail
        birthDate = aBirthDate
        location = aLocation
        profileImage = aProfileImage
    }

    init(parseUser:PFUser) {
        name = parseUser.objectForKey(kParseVariableUserName) as? String
        
        //ADDED BY XAVI
        if ((parseUser.objectForKey(kParseVariableUserGender) as? String) != nil){
            gender = parseUser.objectForKey(kParseVariableUserGender) as? String
        }else{
            gender = ""
        }
        if ((parseUser.objectForKey(kParseVariableUserEmail) as? String) != nil){
            mail = parseUser.objectForKey(kParseVariableUserEmail) as? String
        }else{
            mail = ""
        }
        if ((parseUser.objectForKey(kParseVariableUserBirthDate) as? NSDate) != nil){
            birthDate = parseUser.objectForKey(kParseVariableUserBirthDate) as? NSDate
        }else{
            birthDate = NSDate.init()
        }
        if ((parseUser.objectForKey(kParseVariableUserFacebookId) as? String) != nil){
            facebookId = parseUser.objectForKey(kParseVariableUserFacebookId) as? String
        }else{
            facebookId = ""
        }
        
        //END XAVI ADDITION
        identifier = parseUser.objectId
        pUser = parseUser
        if parseUser.objectForKey(kParseVariableUserFriends) != nil {
            friends = ((parseUser.objectForKey(kParseVariableUserFriends) as? NSArray)?.mutableCopy() as! NSMutableArray)
        } else {
            friends = NSMutableArray()
        }
        profileImageURL = parseUser.objectForKey(kParseVariableUserProfileImage) as? String
        
        if ((parseUser.objectForKey(kParseVariableUserRankPoints) as? NSNumber) != nil){
            rankPoints = (parseUser.objectForKey(kParseVariableUserRankPoints) as? NSNumber)!.integerValue
        }else{
            rankPoints = 0
        }
    }
    
    override init() {
    
    }
    
    
    func fetchFriends() {
            
        for object in self.friends! {
            (object as! PFUser).fetchIfNeededInBackgroundWithBlock({ (friendObject, error) -> Void in
                
                if (error == nil) {
                    self.friends!.addObject(UserModel(parseUser: friendObject as! PFUser))
                }
            })
        }
    }
    
    // MARK: - Get functions
    
    /**
    Get new missions for current user
    
    - parameter successBlock: Block ejecuted when missions returned
    */
    func getNewMissions(successBlock:(missions :NSMutableArray) -> ()) {
        
        DataSource.sharedInstance.getNewMissionsForUser(self, successBlock: { (array) -> () in
            
            successBlock(missions: array)
        })
    }
    
    /**
    Get current missions for current user
    
    - parameter successBlock: Block ejecuted when missions returned
    */
    func getCurrentMissions(successBlock:(missions :NSMutableArray) -> ()) {
        
        DataSource.sharedInstance.getCurrentMissionToUser(self, successBlock: { (array) -> () in
            
            successBlock(missions: array)
        })
    }
    
    func getOwnCurrentMissions(successBlock:(missions :NSMutableArray) -> ()) {
        
        DataSource.sharedInstance.getCurrentMissionFromUser(self, successBlock: { (array) -> () in
            
            successBlock(missions: array)
        })
    }
    
    /**
    Get friends for current user
    
    - parameter successBlock: Block executed when friends returned
    */
    func getFriends(successBlock:(friends :NSMutableArray) -> ()) {
        
        
        DataSource.sharedInstance.getFriendsListForUser(self, withSuccessBlock: { (friendList) -> () in
          
            self.friends = friendList
            successBlock(friends: friendList)
        })
    
    }
    
    
    /**
    Get user friends sorted by rank points for current user
    
    - parameter successBlock: Block executed when friends returned
    */
    func getUserFriendsByPoints(successBlock:(friends :NSMutableArray) -> ()) {
        

        DataSource.sharedInstance.getUserFriendsSortByPoints(self, withSuccessBlock: { (friendList) -> () in

            successBlock(friends: friendList)
        })
        
    }

    
    
    
    func loadProfilePictureWithSuccess(successBlock:() ->()) {
        
        DataSource.sharedInstance.getProfilePictureForLoggedUserWithSuccess(successBlock)
    }

    
    func getParseUser() -> PFUser {
        
        
        self.pUser!.email = self.mail
        
        self.pUser!.setObject(self.name!, forKey: kParseVariableUserName)
        //TODO Check if birth or gender is not nil.
        if (self.gender != nil){
            self.pUser!.setObject(self.gender!, forKey: kParseVariableUserGender)
        }else{
            self.pUser!.setObject("gender not defined", forKey: kParseVariableUserGender)
        }
        if (self.facebookId != nil){
            self.pUser!.setObject(self.facebookId!, forKey: kParseVariableUserFacebookId)
        }else{
            self.pUser!.setObject("Facebook Id not retrieved", forKey: kParseVariableUserFacebookId)
        }
        self.pUser!.setObject(NSDate(), forKey: kParseVariableUserBirthDate)

        
        //Añadido por xavi
        self.pUser!.setObject(self.rankPoints, forKey: kParseVariableUserRankPoints)
        print("Acabo de ponerle los puntos al pUser --> \(self.pUser?.objectForKey(kParseVariableUserRankPoints))")
        
        
        return self.pUser!
    }
    
    func getProfileImageWithSuccess(successBlock:(image:UIImage) -> (), errorBlock:(error:NSError) -> ()) {
        
        let url:NSURL = NSURL(string: self.profileImageURL!)!
        
        downloadImageWithURL(url, andSuccessBlock: { (succeeded, image) -> () in
            
            if (succeeded) {
                successBlock(image: image)
            }
            
        }) { (error) -> () in
            errorBlock(error: error)
        }
    }
    
    func getClossedMissionsWithSuccess(successBlock:(array:NSMutableArray) -> (), errorBlock:(error:NSError) -> ()) {
        
        DataSource.sharedInstance.getClosedMissionsForUser(self, withSuccess: { (results) -> () in
            
            self.clossedMissions = results
            
            successBlock(array: self.clossedMissions!)
            
            }) { (error) -> () in
                
                errorBlock(error: error)
        }
    }
    
    // MARK: - Save functions
    
    /**
    Save user information
    
    - parametersuccessBlock: block executed when succeeded
    - parametererrorBlock: block executed when not succeeded
    */
    func saveInformationWithSuccess(successBlock:() ->(), errorBlock:(error:NSError) -> ()) {
    
        DataSource.sharedInstance.saveUser(self, withSuccess: { () -> () in
            successBlock()
        }) { (error) -> () in
            errorBlock(error: error)
        }
    }
    
    
    func saveUserPointsWithSuccess(successBlock:() ->(), errorBlock:(error:NSError) -> ()){
        
        
        //Trying to call the method that I think adds 2 points to user rankpoints.
        DataSource.sharedInstance.AddPointsToUserWhenAnswer(self, withSuccessBlock: { (user) -> () in
            successBlock()
        })
        
        self.rankPoints += 2
        
    }
    
}
