//
//  AnswerModel.swift
//  IMHO
//
//  Created by Rafael Zamora on 4/12/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import UIKit
import Parse

class AnswerModel: NSObject {
 
    var user:UserModel?
    var mission:MissionModel?
    var comment:String?
    var selectedImage:ImageModel?
    var singleAnswer:Bool?
    var points:Int?
    var createdAt:NSDate?
    var parseAnswer:PFObject?
    
    init (aUser:UserModel, aMission:MissionModel, aComment:String, aImage:ImageModel, aSingleAnswer:Bool, aPoints:Int) {
        
        user = aUser
        mission = aMission
        comment = aComment
        selectedImage = aImage
        singleAnswer = aSingleAnswer
        points = aPoints
    }
    
    init (parseObject:PFObject) {
        
        user = UserModel(parseUser: parseObject.objectForKey(kParseVariableAnswerUser) as! PFUser)
        mission = MissionModel(parseObject: parseObject.objectForKey(kParseVariableAnswerMission) as! PFObject)
        comment = parseObject.objectForKey(kParseVariableAnswerComment) as? String
        selectedImage = ImageModel(aParseImage: parseObject.objectForKey(kParseVariableAnswerSelectedImage) as! PFObject)
        singleAnswer = parseObject.objectForKey(kParseVariableAnswerSingleAnswer)!.boolValue
        points = parseObject.objectForKey(kParseVariableAnswerPoints)!.integerValue
        parseAnswer = parseObject
        
    }
    
    init (parseObject:PFObject, simple:Bool) {
        
        user = UserModel(parseUser: parseObject.objectForKey(kParseVariableAnswerUser) as! PFUser)
        comment = parseObject.objectForKey(kParseVariableAnswerComment) as? String
//        selectedImage = ImageModel(aParseImage: parseObject.objectForKey(kParseVariableAnswerSelectedImage) as! PFObject)
        singleAnswer = parseObject.objectForKey(kParseVariableAnswerSingleAnswer)!.boolValue
        points = parseObject.objectForKey(kParseVariableAnswerPoints)!.integerValue
        createdAt = parseObject.createdAt
        parseAnswer = parseObject
        
    }
    
    func saveInBackgroundWithSuccess(successBlock:(answer :AnswerModel) -> ()) {
        
        DataSource.sharedInstance.saveAnswer(self, withSuccessBlock: { (answer) -> () in
            successBlock(answer: answer)
        })
    }
    
    
}
