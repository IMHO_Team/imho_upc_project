//
//  ImageModel.swift
//  IMHO
//
//  Created by Rafael Zamora on 4/12/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//
/*{ "Version": "2012-10-17", "Statement": [ { "Effect": "Allow", "Action": [ "mobileanalytics:PutEvents", "cognito-sync:*" ], "Resource": [ "*" ] } ] }*/
import UIKit
import Parse

class ImageModel: NSObject {
   
    var type:String?
    var file:PFFile?
    var image:UIImage?
    var status:String?
    var fileName:String?
    var parseImage:PFObject?
    
    init (aParseImage:PFObject) {
        super.init()
        
        parseImage = aParseImage
        file = aParseImage.objectForKey(kParseVariableImageFile) as? PFFile
        fileName = aParseImage.objectForKey(kParseVariableImageFileName)! as? String
    }
    
    init (aFileName:String) {
        fileName = aFileName
    }
    
    func getImageWithBlock(successBlock:(image :UIImage) -> ()) {
        
        if (self.image == nil) {
            downloadImageWithURL(NSURL(string: kDefaultImagesURL+self.fileName!)!, andSuccessBlock: { (succeeded, image) -> () in
                
                self.image = image
                
                successBlock(image: image)
            }) { (error) -> () in
                print("error downloading image");
            }
        } else {
            successBlock(image: self.image!)
        }
    }
    
    func saveImageWithBlock(successBlock:() -> (), errorBlock:(error:NSError) -> ()) {
        
        DataSource.sharedInstance.saveImage(self, withSuccess: { () -> () in
            successBlock()
        }) { (error) -> () in
            errorBlock(error: error)
        }
    }
}
