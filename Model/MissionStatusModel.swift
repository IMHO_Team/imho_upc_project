//
//  MissionStatusModel.swift
//  IMHO
//
//  Created by Rafael Zamora on 4/25/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import UIKit
import Parse

class MissionStatusModel: NSObject {
   
    var name:String?
    var parseMissionStatus:PFObject?
    
    init (parseObject:PFObject) {
        
        name = parseObject.objectForKey(kParseVariableMissionStatus) as? String
        parseMissionStatus = parseObject
    }
}
