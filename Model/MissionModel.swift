//
//  MissionModel.swift
//  IMHO
//
//  Created by Rafael Zamora on 3/22/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import UIKit
import Parse

class MissionModel: NSObject {
    
    var identifier:String?
    var question:String?
    var user:UserModel?
    var images:NSMutableArray?
    var isCompleted:Bool?
    var parseMission:PFObject?
    var objectId:String?
    var category:CategoryModel?
    var isPrivate:Bool?
    var isSecret:Bool?
    var status:String?
    var length:Int?
    var createdAt:NSDate?
    var answers:NSMutableArray?
    var singleUsers:NSMutableArray?
    
    
    
    init(aQuestion:String, aUser:UserModel, anImages:NSMutableArray, aCategory:CategoryModel, isPrivate:Bool, isSecret:Bool, aStatus:String, aLength:Int, users:NSMutableArray) {
        
        self.question = aQuestion
        self.user = aUser
        self.images = anImages
        self.category = aCategory
        self.isPrivate = isPrivate
        self.isSecret = isSecret
        self.length = aLength
        self.status = aStatus
        self.singleUsers = users
    }
    
    init(aQuestion:String, aUser:UserModel, anImages:NSMutableArray, isPrivate:Bool, isSecret:Bool, aStatus:String, aLength:Int, users:NSMutableArray) {
        
        self.question = aQuestion
        self.user = aUser
        self.images = anImages
        self.isPrivate = isPrivate
        self.isSecret = isSecret
        self.length = aLength
        self.status = aStatus
        self.singleUsers = users
    }
    
  

    
    /**
    Create PFObject from model mission
    
    - parameter mission: to be converted to parse object
    */
    func parseMissionFromMissionWithSuccess(successBlock:() -> (), errorBlock:(error:NSError) -> ()) {
        
        self.parseMission = PFObject(className: kParseClassMission)
        
        self.parseMission!.setObject(self.user!.pUser!, forKey: kParseVariableMissionUser)
        self.parseMission!.setObject(self.question!, forKey: kParseVariableMissionQuestion)
        if(self.category != nil){
            self.parseMission!.setObject(self.category!.parseCategory!, forKey: kParseVariableMissionCategory)
        }
        self.parseMission!.setObject(self.isPrivate!, forKey: kParseVariableMissionIsPrivate)
        self.parseMission!.setObject(self.isSecret!, forKey: kParseVariableMissionIsSecret)
        self.parseMission!.setObject(self.length!, forKey: kParseVariableMissionLength)
        self.parseMission!.setObject(self.status!, forKey: kParseVariableMissionStatus)
        
        let usersArray:NSMutableArray = NSMutableArray()
        
        for user in self.singleUsers! {
            
            usersArray.addObject((user as! UserModel).pUser!)
        }
        
        self.parseMission!.setObject(usersArray, forKey: kParseVariableMissionSingleUsers)
        
        let image1:ImageModel = images!.objectAtIndex(0) as! ImageModel
        image1.saveImageWithBlock({ () -> () in
            
            self.parseMission!.setObject(image1.parseImage!, forKey: kParseVariableMissionImage1)
            
            if (self.images?.count > 1) {
                
                let image2:ImageModel = self.images!.objectAtIndex(1) as! ImageModel
                image2.saveImageWithBlock({ () -> () in
                    
                    self.parseMission!.setObject(image2.parseImage!, forKey: kParseVariableMissionImage2)
                    
                    successBlock()
                    
                }, errorBlock: { (error) -> () in
                    errorBlock(error: error)
                })
            } else {
                successBlock()
            }
            
        }, errorBlock: { (error) -> () in
            errorBlock(error: error)
        })
    }

    
    init(parseObject:PFObject) {
        
        parseMission = parseObject
        identifier = parseObject.objectId
        question = parseObject.objectForKey(kParseVariableMissionQuestion) as? String
        objectId = parseObject.objectId!
        user = UserModel(parseUser: parseObject.objectForKey(kParseVariableMissionUser) as! PFUser)
        length = parseObject.objectForKey(kParseVariableMissionLength) as? Int
        status = parseObject.objectForKey(kParseVariableMissionStatus) as? String
        self.images = NSMutableArray()
        self.images!.addObject(ImageModel(aParseImage: parseObject.objectForKey(kParseVariableMissionImage1) as! PFObject))
        if (parseObject.objectForKey(kParseVariableMissionImage2) != nil) {
            self.images!.addObject(ImageModel(aParseImage: parseObject.objectForKey(kParseVariableMissionImage2) as! PFObject))
        }
        
        if ((parseObject.objectForKey(kParseVariableMissionSingleUsers)) != nil) {
            
            self.singleUsers = NSMutableArray()
            
            for object in (parseObject.objectForKey(kParseVariableMissionSingleUsers) as! NSArray) {
                
                self.singleUsers!.addObject(UserModel(parseUser: (object as! PFUser)))
            }
        }
        self.createdAt = parseObject.createdAt
        //let dateFormatter = NSDateFormatter()
        //dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSS'Z'"

    }
    
    func addAnswer(answer:AnswerModel, withSuccess successBlock:(succeeded :Bool) -> ()) {
        
        DataSource.sharedInstance.addAnswer(answer, toMission: self) { (succeeded) -> () in
            successBlock(succeeded: succeeded)
        }
    }
    
    func forwardToUsers(array:NSMutableArray, withSuccess successBlock:() -> (), errorBlock:(error:NSError) -> ()) {
        
        DataSource.sharedInstance.forwardMission(self, toUsers: array, withSuccess: { () -> () in
            
            successBlock()
            
        }) { (error) -> () in
            
            errorBlock(error: error)
        }
    }
    
    func closeMissionWithSuccess(successBlock:() -> (), errorBlock:(error:NSError) -> ()) {
        
        DataSource.sharedInstance.closeMission(self, withSuccess: { () -> () in
            self.status = kParseValueMissionStatusClosed
            successBlock()
            
        }) { (error) -> () in
            
            errorBlock(error: error)
        }
    }
    
    func getMissionAnswersWithSuccessBlock(successBlock:(array:NSMutableArray) ->()) {
        
        DataSource.sharedInstance.getAnswersFromMission(self, successBlock: { (array) -> () in
            
            successBlock(array: array)
        })
    }
    
    // MARK: - Check last answer method
    
    /**
    Checks the user answer if it is the last one. 
    
    - parametersuccessBlock: block executed when succeeded
    */
    func checkInCloudIfLastAnswer(successBlock:() ->(), errorBlock:(error:NSError) -> ()) {
        
        DataSource.sharedInstance.checkInCloudIfLastAnswer(self.identifier!)
        
        
    }

    
    
}
