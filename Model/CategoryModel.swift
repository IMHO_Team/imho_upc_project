//
//  CategoryModel.swift
//  IMHO
//
//  Created by Rafael Zamora on 3/24/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import UIKit
import Parse

class CategoryModel: NSObject {
    
    var name:String?
    var image:ImageModel?
    var parseCategory:PFObject?
    
    init (parseObject:PFObject) {
        parseCategory = parseObject
        name = parseObject.objectForKey(kParseVariableCategoryName) as? String
        image = ImageModel(aParseImage: parseObject.objectForKey(kParseVariableCategoryImage) as! PFObject)
    }
    
    
   
    func getPublicMissionsWithSuccessBlock(successBlock:(array:NSMutableArray) ->()) {
        
        DataSource.sharedInstance.getPublicMissionsForCategory(self, successBlock: { (array) -> () in
            
            successBlock(array: array)
        })
    }
}
