//
//  GroupModel.swift
//  IMHO
//
//  Created by MacBook Pro on 21/3/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

import UIKit

class GroupModel: NSObject {
    var groupName:String?
    var usersCount:Int?
   
    
    
    init(aGroupName: String, aUsersCount:Int) {
        super.init()
        
        groupName = aGroupName
        usersCount = aUsersCount
        
        
    }
   
}
