//
//  SettingData.swift
//  IMHO
//
//  Created by Rafael Zamora on 3/8/15.
//  Copyright (c) 2015 IMHO Enterprises. All rights reserved.
//

// Data lo usamos para objetos locales
// Model lo usamos para objetos de Parse

import UIKit

class SettingData: NSObject {
    
    var name:String?
    var icon:String?
    var cellIdentifier:String?
 
   
    init(aName: String, anIcon:String, aCellIdentifier:String) {
        super.init()
        
        name = aName
        icon = anIcon
        cellIdentifier = aCellIdentifier
        
    }
}
